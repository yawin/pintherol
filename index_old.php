<? require 'globals.php';

if(isset($_GET['cp']))
{
	$userid = $GLOBALS['sesion']->compruebases();
	require 'mens.php';
	
	if($userid>0)
	{
		$ban = $GLOBALS['sesion']->checkBan($userid);

		if($ban == 0)
		{
			require 'realIndex.php';
		}
		else
		{
			ban();
		}
	}
	else
	{
		include 'login.php';
	}
}
else
{
	//Web de visualización?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Pintherol</title>
		<link rel="shortcut icon" href="./img/d20pineado.png">

		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="./dist/css/skins/skin-black-light.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-black-light" OnLoad="OnStart(); checkVersion();">
		<div style="text-align:center;">
			<img style="width:50%;" src="./img/banner.png"/>
		</div>
		<div class="box box-solid">
			<a href="./regis.php" style="margin-right:15px;" class="btn btn-default btn-sm pull-right">Registro</a>
			<a href="./?cp" style="margin-right:5px;" class="btn btn-default btn-sm pull-right">Login</a>
		</div>
		<div id="contenido"></div>
		<div class="box box-solid" style="margin-left: 15px;">
			<strong>Pintherol system (<span id="version">v0.0</span>)</strong> | <strong>Copyright &copy; 2016 <a href="#">Pintherol Team</a>.</strong> All rights reserved.
		</div>
		<script>
			function OnStart()
			{
				$(document).ready(function()
				{
					$.post('./loaderproxy.php',{content:'index', plugin:'inicio'},
					function(output)
					{
						$('#contenido').html(output);
					});
				});
			}
			function checkVersion()
			{
				$(document).ready(function()
				{
					$.post('./loaderproxy.php',{plugin:"acp", content:"ruta", version:''},
					function(output)
					{
						$('#version').html(output);
					});
				});
			}
		</script>
		<!-- jQuery 2.1.4 -->
		<script src="./plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="./bootstrap/js/bootstrap.min.js"></script>
		<!-- AdminLTE App -->
		<script src="./dist/js/app.min.js"></script>
	</body>
	</html>
<?}?>
