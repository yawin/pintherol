<?
error_reporting(E_ALL);
ini_set('display_errors', '1');

	/*
		App Details
			Client ID: 331112287463145472
			Client Secret: WF-MesSSDumY1TOB9CBfWhAZB83BSBng
	*/

	if(!isset($_GET['discord']))
	{
		require('OAuth2/Client.php');
		require('OAuth2/GrantType/IGrantType.php');
		require('OAuth2/GrantType/AuthorizationCode.php');

		$CLIENT_ID = '331112287463145472';
		$CLIENT_SECRET = 'WF-MesSSDumY1TOB9CBfWhAZB83BSBng';

		$REDIRECT_URI = "";
		$API_URL = 'https://discordapp.com/api';
		$AUTHORIZATION_ENDPOINT = $API_URL.'/oauth2/authorize';
		$TOKEN_ENDPOINT = $API_URL.'/oauth2/token';
		$USER_INFO = $API_URL.'/users/@me';
		
		$client = new OAuth2\Client($CLIENT_ID, $CLIENT_SECRET);
		if (!isset($_GET['code']))
		{
			$auth_url = $client->getAuthenticationUrl($AUTHORIZATION_ENDPOINT, array('scope' => 'identify'));
			header('Location: ' . $auth_url);
			die('Redirect');
		}
		else
		{
			$params = array('code' => $_GET['code'], 'redirect_uri' => $REDIRECT_URI, 'client_id' => $CLIENT_ID);
			$response = $client->getAccessToken($TOKEN_ENDPOINT, 'authorization_code', $params);
			parse_str($response['result'], $info);
			$client->setAccessToken($info['access_token']);
			$response = $client->fetch($USER_INFO);
			var_dump($response, $response['result']);
		}
	}
	else
	{
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pintherol | Login con Discord</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="./dist/css/skins/skin-blue-light.min.css">
  </head>
  <body class="hold-transition login-page" On OnLoad="evento();">
	<div id="enerear" class="login-box">
	  <div class="login-logo">
			<a href="./index.php"><img style="width:75%;" src="http://pintherol.hopto.org/imagenes/banner.png"/></a>
	  </div>
	  <div class="login-box-body">
	    <p class="login-box-msg">Sign in to start your session</p>

	      <div class="form-group has-feedback">
	          <a target="_blank" href="./login_discord.php" style="width: 100%;" class="btn btn-primary" OnClick="login();">Login con Discord</a>
	      </div>
	</div>
	<script>
		function login()
		{
			
		}

		function evento()
		{
			$(document).ready(function()
			{
				document.addEventListener("keypress", function(e)
				{
					var key = e.which || e.keyCode;
					if (key === 13)
					{
						login();
					}
				});
			});
		}
	</script>
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
  </body>
</html>
<?}?>
