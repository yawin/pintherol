<?
	function loadModules()
	{
		$ruta = "./pintherol_plugins/";
		if(is_dir($ruta))
		{
			if($dh = opendir($ruta))
			{
				while(($file = readdir($dh)) !== false)
				{
					if(is_dir($ruta . $file) && $file!="." && $file!="..")
					{
						if(file_exists($ruta.$file."/menu.php"))
						{
							require $ruta.$file."/menu.php";
						}
					}
				}
			}
			closedir($dh); 
		}
		else
		{
			echo "Error: No existe el directorio de plugins";
		}
	}
?>

<!DOCTYPE html>
<html>
  <head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Pintherol</title>
	<link rel="shortcut icon" href="./img/d20pineado.png">

	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="./dist/css/skins/skin-black-light.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-black-light sidebar-mini" OnLoad="OnStart(); checkVersion();">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a OnClick="OnStart();" class="logo">
          <img style="width:100%;" src="./img/banner.png"/>
        </a>

        <nav class="navbar navbar-static-top" role="navigation" id="barnav">
			<a OnClick="" class="btn btn-default pull-right" style="margin-top: 10px; margin-right: 5px;" href="./logout.php">Logout</a>
        </nav>
      </header>
      <aside class="main-sidebar">
		<section class="sidebar">
			<ul id="menu" class="sidebar-menu">
				<li class="header">SECCIONES</li>
				<div id="debug"></div>
				<? loadModules();?>
			</ul>
		</section>
      </aside>

      <div id="contenido" class="content-wrapper">
      </div>

      <footer class="main-footer">
		<strong>Pintherol system (<span id="version">v0.0</span>)</strong> | <strong>Copyright &copy; 2016 <a href="#">Pintherol Team</a>.</strong> All rights reserved.
      </footer>
    </div>

    <script>
	function getParameterByName(name)
	{
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	function OnStart()
	{
		plugin = getParameterByName('plugin');
		if(plugin == '')
		{
			plugin = "inicio";
		}
		
		pagina = getParameterByName('page');
		if(pagina == '')
		{
			pagina = 'index';
		}

		_loadPage();
	}

	function loadPage(pag, plug)
	{
		pagina=pag;
 		plugin=plug;
		_loadPage();
	}
	function _loadPage()
	{
		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin},
			function(output)
			{
				$('#contenido').html(output);
			});
		});
	}

	function checkVersion()
	{
		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{plugin:"acp", content:"ruta", version:''},
			function(output)
			{
				$('#version').html(output);
			});
		});
	}
    </script>

    <!-- jQuery 2.1.4 -->
    <script src="./plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="./bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="./dist/js/app.min.js"></script>
  </body>
</html>
