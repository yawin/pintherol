<?
	error_reporting(E_ALL);
	ini_set('display_errors', '1');

	if(isset($_POST['orden']))
	{
		switch($_POST['orden'])
		{
			case 1:?>
				<p>A continuación se muestran los datos de conexión guardados en <b>conexion.php</b>. Si los datos están correctos pulsa "continuar". Si no, edita el fichero y pulsa "recargar".</p>
				<?
					require ("./conexion.php");
				?>
				<div class="alert alert-default">
					<div class="form-group has-feedback">
						<b>Host ($dbhost):</b> <? echo $dbhost;?>
					</div>
					<div class="form-group has-feedback">
						<b>Base de datos ($db):</b> <? echo $db;?>
					</div>
					<div class="form-group has-feedback">
						<b>Base de datos de login ($securdb):</b> <? echo $securdb;?>
					</div>
					<div class="form-group has-feedback">
						<b>Usuario ($dbuser):</b> <? echo $dbuser;?>
					</div>
					<div class="form-group has-feedback">
						<b>Password ($dbpass):</b> <? echo $dbpass;?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-8">
						<i class="btn btn-success pull-right" OnClick="siguiente(1);">Recargar</i>
					</div>
					<div class="col-xs-4">
						<i class="btn btn-primary pull-right" OnClick="siguiente(2);">Continuar</i>
					</div>
					<script>
						function siguiente(sig_orden)
						{
							$(document).ready(function()
							{
								$.post('./install.php',{orden:sig_orden},
								function(output)
								{
									$('#cont').html(output);
								});
							});
						}
					</script>
			</div>

			<? break;
			case 2:
				require ("./conexion.php");
				
				$msg = '';
				try
				{
					$_bd = new PDO('mysql:host='.$dbhost.';dbname='.$db, $dbuser, $dbpass);
				}
				catch(PDOException $e)
				{
					$msg = $e->getMessage();
				}
				try
				{
					$_bd = new PDO('mysql:host='.$dbhost.';dbname='.$securdb, $dbuser, $dbpass);
				}
				catch(PDOException $e)
				{
					$msg = $e->getMessage();
				}
				
				if($msg != '')
				{?>
					<p>Al menos uno de los datos están mal. Comprueba que las bases de datos existen con los nombres indicados y que los datos de conexión son los que están registrados.</p>
					<div class="row">
						<div class="col-xs-8">
							&nbsp;
						</div>
						<div class="col-xs-4">
							<i class="btn btn-danger pull-right" OnClick="siguiente(1);">Volver atrás</i>
						</div>
						<script>
							function siguiente(sig_orden)
							{
								$(document).ready(function()
								{
									$.post('./install.php',{orden:sig_orden},
									function(output)
									{
										$('#cont').html(output);
									});
								});
							}
						</script>
					</div>
				<?}
				else
				{?>
				
					<p>Se han podido realizar las conexiones con éxito</p>
					<div class="row">
						<div class="col-xs-8">
							&nbsp;
						</div>
						<div class="col-xs-4">
							<i class="btn btn-primary pull-right" OnClick="siguiente(3);">Continuar</i>
						</div>
						<script>
							function siguiente(sig_orden)
							{
								$(document).ready(function()
								{
									$.post('./install.php',{orden:sig_orden},
									function(output)
									{
										$('#cont').html(output);
									});
								});
							}
						</script>
					</div>
				<?}
				break;
			case 3:
				require 'conexion.php';
				require 'sesiones.php';

				$query = "
					CREATE TABLE IF NOT EXISTS `files` (
						`id` int(11) NOT NULL,
						`nombre` varchar(50) NOT NULL,
						`url` varchar(200) NOT NULL,
						`partida` int(11) NOT NULL
					) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
					
					CREATE TABLE IF NOT EXISTS `juegos` (
						`id` int(11) NOT NULL,
						`nombre` varchar(50) NOT NULL,
						`sistema` int(11) NOT NULL DEFAULT '0'
					) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

					INSERT INTO `juegos` (`id`, `nombre`, `sistema`) VALUES
					(1, 'El rastro de Cthulhu', 3),
					(2, 'KungFury', 2),
					(3, 'Mundodisco', 1),
					(4, 'La llamada de Cthulhu', 4),
					(5, 'Ãnima, beyond fantasy', 0),
					(6, 'Dungeons & Dragons', 4),
					(7, 'GaÃ±anes', 0),
					(8, 'Roleage', 0),
					(9, 'Cultos Innombrables', 6),
					(10, 'No especificado', 0),
					(11, 'La leyenda de los cinco anillos', 0),
					(12, 'Vampiro: La mascarada', 0),
					(13, 'Inocentes', 0),
					(14, 'Fate', 7),
					(15, 'Dark Cthulhu', 0),
					(16, 'Mago: La ascensiÃ³n', 0),
					(17, 'Reflejo', 0),
					(18, 'Witchcraft (BrujerÃ­a)', 0),
					(19, 'Blacksad', 0),
					(20, 'Fanhunter', 0),
					(21, 'Paranoia', 0);
					
					CREATE TABLE IF NOT EXISTS `partidas` (
						`id` int(11) NOT NULL,
						`nombre` varchar(50) NOT NULL,
						`juego` int(11) NOT NULL,
						`master` int(11) NOT NULL,
						`max_jugadores` int(11) NOT NULL,
						`pegi` int(11) NOT NULL,
						`noobsfriendly` int(11) NOT NULL,
						`abierta` int(11) NOT NULL,
						`sinopsis` varchar(250) NOT NULL
					) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
					
					CREATE TABLE IF NOT EXISTS `sistemas` (
						`id` int(11) NOT NULL,
						`nombre` varchar(50) NOT NULL
					) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
					
					INSERT INTO `sistemas` (`id`, `nombre`) VALUES
					(0, 'Sistema propio'),
					(1, 'GURPS'),
					(2, 'C-System'),
					(3, 'Gumshoe'),
					(4, 'D20'),
					(5, 'D100'),
					(6, 'Hitos'),
					(7, 'Fate'),
					(8, 'Fate acelerado');
					
					CREATE TABLE IF NOT EXISTS `user_tokens` (
						`userid` int(11) NOT NULL,
						`token` varchar(500) NOT NULL
					) ENGINE=MyISAM DEFAULT CHARSET=latin1;
					
					INSERT INTO `user_tokens` (`userid`, `token`) VALUES
					(7, '1c98ca9c860804a5a875cb97c596024eab4e11beacf2034593189f3f4ccd0b7d1ad02df0ea50cffef5badfcb4eff7abd'),
					(8, '36e6a82386a8a28a2e54179c767e1b12d826201a7749ca612386fa3f489f75f8c5e2fdee054c2eb0537e83871a601531'),
					(9, '023f86ebdd36eb06532ab9fc437c4d9a144c74b1fcd05d7de398ed6fe23b5447'),
					(10, '07fc04fd67c902c6694b054b80679394c431f19c'),
					(53, '4fffe6d1'),
					(52, '6e745e12ab765b4662f0cff384b74378'),
					(51, 'cdb15c03'),
					(50, '6f4b5984af20a6b75cbd2d9e71d88926'),
					(49, 'd86f43c8a368f1d44d140c6452c8da3acfaf13e55df57b19740682d7b606694f8895ed069bc8c95052df982b68517d2c'),
					(48, 'c0ab1f4d'),
					(46, '7b720630'),
					(47, 'c562252d07003b1504c8f8d4c2397b19'),
					(44, 'd5d42965cd9158e1aa97908b1f2901c6'),
					(45, '2c0c6f0f'),
					(43, '871b185ec80c1def3bd19ce22c5eb2106c521e7d26f25fe96459ec7ae6f454397076a0aed87f6f5a5cd0012d7e25be4f'),
					(42, 'f9d6eb106b29cc84d2cede71caa35c08'),
					(41, '5424dc411a0cba69a5e6422b587893f3'),
					(40, '8104ae81668758344a8f223a4e7e923b545ee39bcab12d7fab7ad98a3bd6e7ab'),
					(39, '5143cf861dec64ffdbaf0cbae1765cf7'),
					(38, 'b122e86b'),
					(37, '3bde28ce24612a17cbc77e640fb2c03259c57a7a0e7ed0215d916b9f2dc317e0bf9e1e3fae49050a6597444a56a0923da6fc8c4bcebaa9723fa2fe8d7ddd5ba8'),
					(35, 'dfae9722'),
					(36, '075bf815ab59bab3672a5f33c1df2b4e158054b9bebd7b579e80b04f9d8255d39bf3b030d4cee7a05b18a371c7dc292a'),
					(34, '040e23cab781a53c02b8d798c763f1b82825f0ac0db563a156519d7b46ef1bed'),
					(32, 'a0b8fe6f72b8f577951d42c5fa45aaaa68e8b3fe'),
					(33, 'fae3c68dab84ea85c02b638db4ac02ab'),
					(31, '84c7cce086ba6277494c960119b648ca740643acee044f178a99a969b463de88dfcb5ded926b8183991041d0d3d449dc'),
					(30, '08a11f6f60501f8d1a27930b40551ca08c000670'),
					(29, '7615bd81a7d1b473ca664d7df64f19a3369f434d55309b64e779dfcabb7e71e1ae0162c3622b0550aa81ab43d8e20b0f05630496272bea97f412d78fbd2740fb'),
					(28, 'ae1c69ea88a6e9834fb0e5190d3e55597696d541d1aed84dcda490a189d50d87'),
					(27, 'ea97996066b955c5e7e61c1f40bbba192e0b9b10'),
					(26, 'ae1744cdf812a3af7e21f778ebbe92d7'),
					(25, '27ccc5d7b9a1e3f31af0446300453ea31c4974c51b8ae12c860d7de83fd06ada'),
					(24, '67b357a5b284d39b41d1917e659c3149'),
					(23, '0eeb27ee7ef6953f62163316af934b76971f054c'),
					(22, '59cbfb18'),
					(21, '6a26d7ed30aad2b4f371610a5e735b1c'),
					(20, '531d66a42b03c578fd4d0e520f5443dfc7b4af19486c48a9b193197e9eff573b'),
					(19, '54ec3d1405ac98d42e0c62216220b5f5b69c60d59ef1978f5a5810bd472f086e'),
					(18, '22d3ccc3e6b79fd5a6cc8652a36eb39f34dde225a37d997b60a80a4c681085eab3d3f11781522ae8d21101c866b02560b15969263a08604d7c16627e63652e9a'),
					(17, '4cbee4b9'),
					(16, '1e69e8ea6cf13fc9f84add03cc69620d'),
					(15, '79747a2f022812374b868b4fb48ce31d2887caa3da50c38623e0e51424fa007a'),
					(14, '0171d79f72ab5fee92ebdd3b29cca8a7'),
					(13, '212532ffd6c2089728543e9121672f9e'),
					(12, '928eb8c36b0a15067b7f1c51086d983a'),
					(11, 'c929f02aa2f7a214d74e010e2b35cf49');
					
					CREATE TABLE IF NOT EXISTS `users` (
						`id` int(11) NOT NULL COMMENT 'Id de usuario',
						`username` varchar(25) NOT NULL COMMENT 'Nombre de usuario',
						`discord` varchar(50) NOT NULL COMMENT 'Email del usuario',
						`ban` tinyint(1) DEFAULT '0' COMMENT '¿Está baneado el usuario?',
						`privilegios` int(11) NOT NULL DEFAULT '0' COMMENT 'Privilegios del usuario',
						`avatar` varchar(255) NOT NULL COMMENT 'Avatar del usuario',
						`bloq` tinyint(1) NOT NULL DEFAULT '0'
					) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
					
					INSERT INTO `users` (`id`, `username`, `discord`, `ban`, `privilegios`, `avatar`, `bloq`) VALUES
					(1, 'yawin', 'Yawin#3164', 0, 101, '', 0),
					(7, 'Amaiuki', 'Amaiuki#3164', 0, 1, '', 0),
					(8, 'user', 'user#3164', 0, 0, '', 0),
					(9, 'master', 'master#3164', 0, 1, '', 0),
					(10, 'admin', 'admin#3164', 0, 101, '', 0),
					(11, 'Fausto', '0', 0, 1, '', 0),
					(12, 'Baldellou', '0', 0, 1, '', 0),
					(13, 'JoeyGumer', '0', 0, 1, '', 0),
					(14, 'AidenBlackangel', '0', 0, 1, '', 0),
					(15, 'Javichan', '0', 0, 1, '', 0),
					(16, 'Mogu', '0', 0, 1, '', 0),
					(17, 'Catorce18', '0', 0, 1, '', 0),
					(18, 'murogameplay', '0', 0, 1, '', 0),
					(19, 'Caligine', '0', 0, 1, '', 0),
					(20, 'Vlad Temper', '0', 0, 1, '', 0),
					(21, 'kaigosha', '0', 0, 1, '', 0),
					(22, 'cwolf', '0', 0, 1, '', 0),
					(23, 'Kili', '0', 0, 1, '', 0),
					(24, 'Varso', '0', 0, 1, '', 0),
					(25, 'Rainitis', '0', 0, 1, '', 0),
					(26, 'Xen Art', '0', 0, 1, '', 0),
					(27, 'Rayner', '0', 0, 1, '', 0),
					(28, 'Cer0', '0', 0, 1, '', 0),
					(29, 'Astur', '0', 0, 1, '', 0),
					(30, 'Da Silva', '0', 0, 1, '', 0),
					(31, 'Harbesi', '0', 0, 1, '', 0),
					(32, 'Banana', '0', 0, 1, '', 0),
					(33, 'eldaikno', '0', 0, 1, '', 0),
					(34, 'Cárdinal Varsen', '0', 0, 1, '', 0),
					(35, 'Shinjiro', '0', 0, 1, '', 0),
					(36, 'Henryxperia', '0', 0, 1, '', 0),
					(37, 'ancarix96', '0', 0, 1, '', 0),
					(38, 'joel', '0', 0, 1, '', 0),
					(39, 'LagiaDOS', '0', 0, 1, '', 0),
					(40, 'Gabrile', '0', 0, 1, '', 0),
					(41, 'Niins', '0', 0, 1, '', 0),
					(42, 'iKKi', '0', 0, 1, '', 0),
					(43, 'Niebla', '0', 0, 1, '', 0),
					(44, 'Androjebac', '0', 0, 1, '', 0),
					(45, 'Moose', '0', 0, 1, '', 0),
					(46, 'Pedro Lastiri', '0', 0, 1, '', 0),
					(47, 'Kyôjin', '0', 0, 1, '', 0),
					(48, 'Gwynsteoporosis', '0', 0, 1, '', 0),
					(49, 'Duajunior30', '0', 0, 1, '', 0),
					(50, 'Nacimsantos', '0', 0, 1, '', 0),
					(51, 'quawtum', '0', 0, 1, '', 0),
					(52, 'culoextremo', '0', 0, 1, '', 0),
					(53, 'Danz', '0', 0, 1, '', 0);
					
					CREATE TABLE IF NOT EXISTS `users_en_partida` (
						`userid` int(11) NOT NULL,
						`partidaid` int(11) NOT NULL,
						`aceptado` int(11) NOT NULL DEFAULT '0'
					) ENGINE=InnoDB DEFAULT CHARSET=latin1;
					
					ALTER TABLE `files`
					 ADD PRIMARY KEY (`id`);

					ALTER TABLE `juegos`
					 ADD PRIMARY KEY (`id`);

					ALTER TABLE `partidas`
					 ADD PRIMARY KEY (`id`);

					ALTER TABLE `sistemas`
					 ADD PRIMARY KEY (`id`);

					ALTER TABLE `user_tokens`
					 ADD PRIMARY KEY (`userid`);

					ALTER TABLE `users`
					 ADD PRIMARY KEY (`id`);

					ALTER TABLE `users_en_partida`
					 ADD PRIMARY KEY (`userid`,`partidaid`);

					ALTER TABLE `files`
					 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

					ALTER TABLE `juegos`
					 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;

					ALTER TABLE `partidas`
					 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

					ALTER TABLE `sistemas`
					 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;

					ALTER TABLE `users`
					 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de usuario',AUTO_INCREMENT=54;
					 
					 CREATE TABLE IF NOT EXISTS `hoja_de_ruta` (
						`id` int(11) NOT NULL,
						`version` int(11) NOT NULL,
						`subversion` int(11) NOT NULL,
						`tarea` varchar(255) NOT NULL,
						`estado` int(11) NOT NULL
					) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

					INSERT INTO `hoja_de_ruta` (`id`, `version`, `subversion`, `tarea`, `estado`) VALUES
					(1, 1, 0, 'Tabl&oacute;n de partidas', 2),
					(2, 1, 1, 'Sistema de login', 2),
					(3, 1, 2, 'Panel para crear nuevas partidas', 2),
					(4, 2, 0, 'Reprogramar la web con las caracter&iacute;sticas actuales', 2),
					(5, 2, 1, 'Habilitar registros', 2),
					(6, 2, 2, 'Panel de control de usuario', 2),
					(7, 2, 3, 'Panel de control de administrador', 2),
					(8, 2, 4, 'Paneles para a&ntilde;adir juegos y sistemas', 2),
					(9, 2, 5, 'Panel de administraci&oacute;n de partidas', 2),
					(10, 2, 6, 'Posibilidad de que los usuarios se unan a partidas y los masters los aprueben', 2),
					(11, 2, 7, 'Posibilidad para que los masters a&ntilde;adan documentos a la partida', 2),
					(12, 2, 8, 'Posibilidad de que los masters abran y cierren las inscripciones a las partidas', 2),
					(13, 2, 9, 'Script de instalaci&oacute;n de Pintherol', 2),
					(14, 2, 10, 'Sistema de hoja de ruta para administrar tareas', 1);

					ALTER TABLE `hoja_de_ruta`
					 ADD PRIMARY KEY (`id`);

					ALTER TABLE `hoja_de_ruta`
					 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
				";
				$bd->insert($query);
				
				$securbd = new DB($dbhost, $securdb, $dbuser, $dbpass);
				$query = "
					CREATE TABLE IF NOT EXISTS `secur` (
						`id` int(11) NOT NULL,
						`username` varchar(120) NOT NULL,
						`pass` varchar(500) NOT NULL,
						`semilla` varchar(120) NOT NULL
					) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
					
					INSERT INTO `secur` (`id`, `username`, `pass`, `semilla`) VALUES
					(1, '6200eccce3f8bf2a89fd13c9995a1810', '809a2aa6b72d86e15ac5002e7b749416', '38847884096118320550'),
					(7, '3e95f768515588614f19c456e37d2022', '805444a000f451a101a8d13ecbd268c5f2039f5d5f9b0f7930e6b4a3967b9575bab0ba16f094574db04b2e341448e133', '13796293828498'),
					(8, 'ee11cbb19052e40b07aac0ca060c23ee', '08b7ec087b60b5055c54450e252a731e0e3cb67faff889521017fced704cb6a9', '716523096314294522'),
					(9, 'eb0a191797624dd3a48fa681d3061212', '47d61255', '115005638175'),
					(10, '21232f297a57a5a743894a0e4a801fc3', 'c7f1205588bbb06f286028ff3ccefac2b42ccdba', '42669042976563'),
					(11, '5e064a44ab768f9e0d5a7e0639cc16e7', '331d70aacb91e0f80b2d97a8a55194a0', '1160312609'),
					(12, 'b458d4bd8d661b991a8da87ac4bf0b2d', '497578d40f613166a433297092fdc6c106467a52464a86f643c48ff6a5172dbb', '5610519220490813512'),
					(13, '5734ba7fc2e5626edf14108bbb52aa87', 'ad56e402b3432876a6dd0476d1dad53deff2f6eb0b4312b64cdc0595961e62d2', '6317817620133742737'),
					(14, '95783e2974939a8458880e9acb4f1ff0', '99db374c', '20292316691874'),
					(15, 'd4530037cb2e07fd5a224b8c4cb25173', '87dc230d', '824066725812384'),
					(16, 'a50c4c65dbf8ab9baf1e307c20e013cd', '693d995cadedf9bf88f7bae6a5dda25c64c76c62976c3d35799a7bea56c7f110be6620259446582dd9c171de910133cc549f473fe65b44170c571952000680ab', '69975809866'),
					(17, '70d241e27721dff4424c48e9362c665b', 'c47fd52f8887fb60403196a04b43369f8ebd1bea810ad0669169953ce03175a21e74b2fb5d2b29ca0859b4e2d41dc754', '785750560426028'),
					(18, '19c225119959c00d8aca8642204e6111', '4489728e218bf8682a138e23a6e4ce7a04cd2ee7885251052a7e17c68b1dcccad5e7e1410e9e72095f949f8812d0b8f4', '31036631991188'),
					(19, 'b86a0970f541e72607b353d2ce3f4d35', '5306b8bc9c416af1dda92035aaea4d968a834440636a3ca90881995c86bc8968', '00720197506527'),
					(20, '08a28eab4f728a6a645f92cdbda9e0e2', '8e455f7f', '0691422619115642944'),
					(21, 'dfe574a09fc5c2df71396ce1e5cc3152', '46fecd06a366a3da618d1d42da4c6cfbec881c07710a26b6a4e359c672cfaabe78ae80010191902d55b4228f524cdb64fdad408192bd8454afe0adf7259b6f4d', '3059150306736'),
					(22, '10d0736f7e20e6492208e3a94351f4ee', 'cb8179a0d6978bee7517a629ec737875b7293b762333781f2c5fb97ba55993c76217c434e089bc0c0791c33c1056f8ab', '33306602527558'),
					(23, '2a0c7e16eb3a1d54d7ba4f0179646ded', '80236c52', '05938244956474'),
					(24, '1fa46eecf01d8eee6a4baf2fab6234f6', 'b06d71abf7302693bd2571d106dffc59', '3799140643468963611'),
					(25, '0843cc6bd04a0db37a8a78dc50e1ba29', 'ec750bae', '247238735'),
					(26, 'fcbfba473c782f4beb34f7383836b8c9', '74fc11caecd487b9d6050ed6cbe5c0abea72d9b936d3037744f6f8bd0a2739b9', '677194934959782'),
					(27, 'c29eb881ff8f9d15b51ba1de4040412a', '616820bc5474da98adfa85daeffddea40a3e6d4c9eb96d208524a2f4bae5a4684db0879e8ad721cb47adef0d0f80aebe', '729213618'),
					(28, '3c2057409a01a43df8bde2521df7dc49', 'c69eb7fc7938777a126be7c35e8c45153b04d1f7', '4364194417036623'),
					(29, 'fac39391a9a3ed976313a3884e5d59e4', '7f1390c5f5ff28545d150e8c26c30eb449a7b3f6503a80492181436e65876c8b', '42146554681305981047'),
					(30, '148551f0075a4421c73cc26a0cddb20d', '910d54b1c6c39b33946490d05934ba3834870d0a998c38bb731a94e9d83e30694827fc3acb1f93796a5a28a919033008', '584240618'),
					(31, '115c81d1df9e6c0f7eb2b64724252e2a', '5f662b3357767f32cfdabe0ee0ccdfa321ecd062b7a05f446f1c7fce46fa6e78', '433385781879362747'),
					(32, 'e6f9c347672daae5a2557ae118f44a1e', '6f84893ac799abe9941bff8ef880fca1', '123408190'),
					(33, 'f68627d85d9cadb2193528d41da13b9b', '5e4d9927', '013892418210554'),
					(34, '2fa1e105f1329f9bda9f4e293fa164e7', '57ae1f33d2ec864e3d669667f72b94f8de6957a2694cf397da09437e992c77fc', '294436368313877'),
					(35, 'b51fca83e6f7ff3d8ae781ccf8d3b9e4', '902955f5c6c2eb08ec5da71b395ce026', '8763127800'),
					(36, 'd15be76b7da3befa42200c72392b4648', '0b14c712e84297da407623ed7962692b', '1153321431'),
					(37, 'b43883168e744601545901f1cf8b4bbe', '9bbe0bf9a552fb262932657f619843e4', '8923366409'),
					(38, 'c000ccf225950aac2a082a59ac5e57ff', '09dde7fe23a09e4a5494972aa57f6fd7', '438518851'),
					(39, '4992f5b652ac6d130acba84b885e45d9', '0c8bb7e8', '2630234510709314'),
					(40, 'f8ce88dad055b1f1507e0360b3b88af3', '34212d063423dc47f6312452af5c174618f9cacd337a0714355d769dbee93ade0b5b030766c69bbeaa174ae07749e7aaf2c3e175b8910551f1dc2a6d815c5602', '530875396'),
					(41, 'fe462379f430822627200dc21d027682', '82f75b97d139775b6919d0d56da5b771', '3212759669290'),
					(42, 'ee37b25ba819f38ae7727b9ac5ded27a', '998808c3', '7057521943129095'),
					(43, '3a522f4d8601fee5542185ba24a8cdfd', '42ec4ee4f85fd1d169310b7f898b2d14', '373954439759'),
					(44, '8f85235b27b16ca19472b3d5f846283a', '9d4eb46398a9680a2977d6fad678dae0b2bb7bc3', '0590727113'),
					(45, '030acfd68d0c19e8b0f891a8c0b525a1', '18e020c9539b7c82598088a9b8aec578', '23033534351'),
					(46, '6cb9dba1bef8497d109b49932fd4f59d', '34977b1b7bf5c490f79ad95a67fbfffb601c0cde30e74dfc87a074b2295aec69', '847178514857472'),
					(47, '10deb827ce4a0b3b0dad6a7f7a5f73a7', 'd039338a477fd31eb2e5914d94bb8bea4b46bca1410c63661618757a18d90ed5bf1d9c2640cbb3eba14d7a9dd02d9a0fbccde046fa596182e633ec512da193c9', '9035972041665513996'),
					(48, 'fc8275031d10053616f9d71370d7a2e2', 'f840629db201782a7eecec9afba101a302c99860ee4bef0d066c64b407a5186b', '2147020172'),
					(49, '0072f6560a464ae2fd3d2112aa512501', '8022c198', '172642854'),
					(50, '4b13aea9302ef77d4fa4ccfd9023d092', '88a31533f264fead3466200d9315dbf4', '081482119'),
					(51, '878b9f75ded3363f20820fafad26f591', '9bd0cf0e47128bd59a8463d51ad91ad355302726106db4b121691a61b36b58f9357a618cfac798ab312f64cab65b0827', '89097146467321159458'),
					(52, '5d98f031901deedb71a24637182dc42b', 'ef570bf7', '51194309254035'),
					(53, '5edab9174ce76f42164be27f301aa7e6', '76cf132c', '1406626056972475');

					CREATE TABLE IF NOT EXISTS `sessiones` (
						`id` int(11) NOT NULL,
						`ip` varchar(15) CHARACTER SET utf8 NOT NULL COMMENT 'IP del cliente',
						`nvar` varchar(2500) CHARACTER SET utf8 NOT NULL COMMENT 'Nombre de la variable de sesión',
						`nval` varchar(2500) CHARACTER SET utf8 NOT NULL COMMENT 'Valor de la variable de sesión',
						`userid` int(11) NOT NULL
					) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

					ALTER TABLE `secur`
					 ADD PRIMARY KEY (`id`);

					ALTER TABLE `sessiones`
					 ADD PRIMARY KEY (`id`);
 
					ALTER TABLE `secur`
					 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;

					ALTER TABLE `sessiones`
					 MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
				";
				$securbd->insert($query);?>
				
				<p>Base de datos creada</p>
				<div class="row">
					<div class="col-xs-8">
						&nbsp;
					</div>
					<div class="col-xs-4">
						<i class="btn btn-primary pull-right" OnClick="siguiente(5);">Continuar</i>
					</div>
					<script>
						function siguiente(sig_orden)
						{
							location.href ="./index.php?cp";
						}
					</script>
				</div>
				<? break;
			case 4:?>
			<p class="tip">Introduce los datos de la cuenta de administrador</p>      
				<div class="box box-solid">
					<p>
						<label>
							<strong>Usuario</strong><br/>
							<input type="text" id="usuario" style="width:280px; height:25px;" size="20" maxlength="20"/>
						</label>
					</p>
					<p>
						<label>
							<strong>Contrase&nacute;a</strong><br/>
							<input type="password" size="20" style="width:280px; height:25px;" id="password" maxlength="20"/>
						</label>
					</p>
					<p>
						<label>
							<strong>Repite contrase&nacute;a</strong><br/>
							<input type="password" size="20" style="width:280px; height:25px;" id="rpassword" maxlength="20"/>
						</label>
					</p>
				</div>
				<div class="row">
					<div class="col-xs-8">
						&nbsp;
					</div>
					<div class="col-xs-4">
						<i class="btn btn-primary pull-right" OnClick="siguiente(5);">Continuar</i>
					</div>
					<script>
						function siguiente(sig_orden)
						{
							username = $("#usuario").val();
							password = $("#password").val();
							rpassword = $("#rpassword").val();

							$(document).ready(function()
							{
								$.post('./install.php',{orden:sig_orden, usuario:username, password:password, rpassword:rpassword},
								function(output)
								{
									$('#cont').html(output);
								});
							});
						}
					</script>
				</div>
				<? break;
			case 5:
			
			require "globals.php";
			$discord = "No se ha introducido";
			if(isset($_POST['usuario']))
			{
				if(trim($_POST["usuario"]) != "" && trim($_POST["password"]) != "" && trim($_POST["rpassword"]) != "")
				{
					$username=strtolower(htmlentities($_POST["usuario"], ENT_QUOTES));
					$pass=$_POST["password"];
					$rpass=$_POST["rpassword"];

					if($pass==$rpass)
					{
						$query = 'SELECT count(id) FROM users WHERE username=\''.$username.'\'';
						foreach($bd->select($query) as $ros)
						{
							if($ros[0]==0)
							{
								$cont = rand(9,20);
								$semilla = "";
								for($i=0;$i<$cont;$i++)
								{
									$semi=rand(0,9);
									$semilla=$semilla.$semi;
								}
								require './cript.php';
								$contras=jarl($pass,$semilla);

								$GLOBALS['sesion']->set_login($username, $contras, $semilla);

								$query = 'INSERT INTO users (username,discord,privilegios) VALUES (\''.$username.'\',\''.$discord.'\',0)';
								$bd->insert($query);

								$query = 'SELECT id FROM users WHERE discord=\''.$discord.'\'';
								foreach($bd->select($query) as $row)
								{
									$cont=rand(9,20);
									$semilla="";
									for($i=0;$i<$cont;$i++)
									{
										$semi=rand(0,9);
										$semilla=$semilla.$semi;
									}
									
									$tok=jarl($semilla,$semilla);
									$query = 'INSERT INTO user_tokens (userid,token) VALUES ('.$row['id'].',\''.$tok.'\')';
									$bd->select($query); 
								}?>
								<p>Cuenta registrada con éxito</p>
								<div class="row">
									<div class="col-xs-8">
										&nbsp;
									</div>
									<div class="col-xs-4">
										<i class="btn btn-primary pull-right" OnClick="siguiente(5);">Continuar</i>
									</div>
									<script>
										function siguiente(sig_orden)
										{
											location.href ="./index.php?cp";
										}
									</script>
								</div>
							<?}
							else
							{?>
								<p><span style="color:red;">*El usuario ya existe</span></p>
								<div class="row">
									<div class="col-xs-8">
										&nbsp;
									</div>
									<div class="col-xs-4">
										<i class="btn btn-danger pull-right" OnClick="siguiente(4);">Volver atrás</i>
									</div>
									<script>
										function siguiente(sig_orden)
										{
											$(document).ready(function()
											{
												$.post('./install.php',{orden:sig_orden},
												function(output)
												{
													$('#cont').html(output);
												});
											});
										}
									</script>
								</div>
							<?}
						}
					}
					else
					{?>
						<p><span style="color:red;">*Las contrase&ntilde;as no coinciden</span></p>
						<div class="row">
							<div class="col-xs-8">
								&nbsp;
							</div>
							<div class="col-xs-4">
								<i class="btn btn-danger pull-right" OnClick="siguiente(4);">Volver atrás</i>
							</div>
							<script>
								function siguiente(sig_orden)
								{
									$(document).ready(function()
									{
										$.post('./install.php',{orden:sig_orden},
										function(output)
										{
											$('#cont').html(output);
										});
									});
								}
							</script>
						</div>
					<?}
				}
				else
				{?>
					<p><span style="color:red;">*Debes rellenar todos los campos</span></p>
					<div class="row">
						<div class="col-xs-8">
							&nbsp;
						</div>
						<div class="col-xs-4">
							<i class="btn btn-danger pull-right" OnClick="siguiente(4);">Volver atrás</i>
						</div>
						<script>
							function siguiente(sig_orden)
							{
								$(document).ready(function()
								{
									$.post('./install.php',{orden:sig_orden},
									function(output)
									{
										$('#cont').html(output);
									});
								});
							}
						</script>
					</div>
				<?}
			}
			break;
		}
	}
	else
	{?>
		<html>
			<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<title>Pintherol | Instalación</title>

				<!-- Tell the browser to be responsive to screen width -->
				<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
				<!-- Bootstrap 3.3.5 -->
				<link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
				<!-- Font Awesome -->
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
				<!-- Ionicons -->
				<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
				<!-- Theme style -->
				<link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
				<link rel="stylesheet" href="./dist/css/skins/skin-blue-light.min.css">
			</head>
			<body class="hold-transition login-page">
				<div class="login-box">
					<div class="login-logo">
							<a href="./index.php"><img style="width:75%;" src="http://pintherol.hopto.org/imagenes/banner.png"/></a>
					</div>
					<!-- /.login-logo -->
					<div class="login-box-body">
						<p class="login-box-msg" style="font-size: 25px;">Instalación de Pintherol</p>
						<div id="cont">
							<p>Pulsa "continuar" para comenzar el proceso de instalación de Pintherol.</p>
							<div class="row">
								<div class="col-xs-8">
									<i class="btn btn-primary pull-right" OnClick="siguiente(1);">Continuar</i>
								</div>
								<script>
									function siguiente(sig_orden)
									{
										$(document).ready(function()
										{
											$.post('./install.php',{orden:sig_orden},
											function(output)
											{
												$('#cont').html(output);
											});
										});
									}
								</script>
							</div>
						</div>
						<div class="text-center" id="login_info">
							<br/>
						</div>
						<div class="text-center" style="margin-top: 5px;">
							<strong>Copyright &copy; 2016 <a href="#">Pintherol Team</a>.</strong> All rights reserved.
						</div>
					</div>
				<!-- /.login-box-body -->
				</div>
				<!-- /.login-box -->
				<!-- jQuery 2.1.4 -->
				<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
				<!-- Bootstrap 3.3.5 -->
				<script src="bootstrap/js/bootstrap.min.js"></script>
				<!-- AdminLTE App -->
				<script src="dist/js/app.min.js"></script>
			</body>
		</html>
	<?}
?>
