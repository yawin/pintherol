<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pintherol | Términos y condiciones de uso</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="./dist/css/skins/skin-blue-light.min.css">
  </head>
  <body class="hold-transition skin-black-light sidebar-mini">
	<div class="content">
		<div class="col-md-8">
			<img style="width:75%;" src="http://pintherol.hopto.org/imagenes/banner.png"/>
			<div class="box box-solid">
				<!-- /.login-logo -->
				<div class="box-body">
					<style>
						h3
						{
							margin-bottom:-5px;
						}
						h1
						{
							margin-top:-8px;
							color:black;
						}
					</style>

					<h1>T&eacute;rminos y Condiciones de Uso</h1>
					<div style="height:505px; overflow-y: scroll; margin-bottom:-15px;">
						<p>1- Todo contenido legalmente clasificado como para mayores de edad, queda totalmente prohibido. Esto incluye material pornográfico, narcóticos, contenido visceral, etc.</p>

						<p>2- Está totalmente prohibido revelar información privada de terceros (fotos, teléfonos, formas de contacto, etc).</p>

						<p>3- Obedecemos unas normas cívicas básicas, esto es: nada de insultar o hacer comentarios hirientes, hablar siempre comprendiendo que todos tenemos libertad de expresión y tener un mínimo de respeto por las personas que se encuentras al otro lado de la pantalla.</p>

						<p>4- Está prohibido el registro de cuentas con datos falsos así como las multicuentas.</p>

						<p>5- Cada master es responsable del contenido de sus partidas.</p>
						
						<p>6- Pintherol (en adelante "La Web") no se hace responsable del contenido de las partidas, juegos de rol y comentarios de quienes hagan uso de la web.</p>
					</div>
				</div>
			</div>
		</div>
	  </div>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
  </body>
</html>
