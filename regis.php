<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pintherol | Registro</title>
	<link rel="shortcut icon" href="./img/d20pineado.png">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="./dist/css/skins/skin-blue-light.min.css">
  </head>
  <body class="hold-transition login-page" OnLoad="checkVersion();">
	<div class="login-box">
	  <div class="login-logo">
			<a href="./index.php"><img style="width:75%;" src="./img/banner.png"/></a>
	  </div>
	  <!-- /.login-logo -->
	  <div class="login-box-body">
		<?  require './globals.php';
			$formulario = 1;

			if(isset($_POST['usuario']))
			{
				if(trim($_POST["usuario"]) != "" && trim($_POST["password"]) != "" && trim($_POST["rpassword"]) != "" && trim($_POST["discord"]) != "")
				{
					$username=htmlentities($_POST["usuario"], ENT_QUOTES);
					$pass=$_POST["password"];
					$rpass=$_POST["rpassword"];
					$discord=$_POST["discord"];

					if($pass==$rpass)
					{
						/*if (filter_var($mail, FILTER_VALIDATE_EMAIL))
						{*/
							if(isset($_POST['tos']))
							{					
								$query = 'SELECT count(id) FROM users WHERE username=\''.$username.'\'';
								foreach($bd->select($query) as $ros)
								{
									if($ros[0]==0)
									{
										$query = 'SELECT count(id) FROM users WHERE discord=\''.$discord.'\'';		
										foreach($bd->select($query) as $ros2)
										{
											if($ros2[0]<1)
											{
												$cont = rand(9,20);
												$semilla = "";
												for($i=0;$i<$cont;$i++)
												{
													$semi=rand(0,9);
													$semilla=$semilla.$semi;
												}
												require './cript.php';
												$contras=jarl($pass,$semilla);

												$GLOBALS['sesion']->set_login($username, $contras, $semilla);
												
												$query = 'INSERT INTO users (username,discord,privilegios) VALUES (\''.$username.'\',\''.$discord.'\',0)';
												$bd->insert($query);

												$query = 'SELECT id FROM users WHERE discord=\''.$discord.'\'';
												foreach($bd->select($query) as $row)
												{
													$cont=rand(9,20);
													$semilla="";
													for($i=0;$i<$cont;$i++)
													{
														$semi=rand(0,9);
														$semilla=$semilla.$semi;
													}
													/*$tok=jarl($semilla,$semilla);
													$query = 'INSERT INTO user_tokens (userid,token) VALUES ('.$row['id'].',\''.$tok.'\')';
													$bd->select($query);*/

													/*$asunto="Verifica tu cuenta";
													$mensaje="�Gracias por registrarte en Pintherol! Para completar el registro necesitamos que verifiques que este email es tuyo. Para ello pulsa en el siguiente enlace: <a href=\"http://pintherol.dnns.net?token=".$tok."\">http://pintherol.dnns.net?token=".$tok."</a>";
				
													$mensaje = str_replace('\"', '"', $mensaje);

													enviamail($mail, $asunto, $mensaje);*/
													require 'mens.php';
													seenvmail();
													$formulario = 0;
												}
											}
											else
											{?>
												<p><span style="color:red;">*Ya hay un usuario con ese ID de Discord</span></p>
											<?}
										}
									}
									else
									{?>
										<p><span style="color:red;">*El usuario ya existe</span></p>
									<?}
								}
							}
							else
							{?>
								<p><span style="color:red;">*Debes aceptar los T&eacute;rminos y Condiciones de uso</span></p>
							<?}
						/*}
						else
						{?>
							<p><span style="color:red;">*El mail no es v�lido</span></p>
						<?}*/
					}
					else
					{?>
						<p><span style="color:red;">*Las contrase&ntilde;as no coinciden</span></p>
					<?}
				}
				else
				{?>
					<p><span style="color:red;">*Debes rellenar todos los campos</span></p>
				<?}
			}

			if($formulario == 1)
			{?>
				<p class="tip">Rellena todos los campos para registrarte</p>      
				<form action="regis.php" id="login-form" method="post">
					<div class="box box-solid">
						<p>
							<label>
								<strong>Usuario</strong><br/>
								<input autofocus type="text" name="usuario" style="width:280px; height:25px;" size="20" maxlength="20"/>
							</label>
						</p>
						<p>
							<label>
								<strong>Contrase&nacute;a</strong><br/>
								<input type="password" size="20" style="width:280px; height:25px;" name="password" maxlength="20"/>
							</label>
						</p>
						<p>
							<label>
								<strong>Repite contrase&nacute;a</strong><br/>
								<input type="password" size="20" style="width:280px; height:25px;" name="rpassword" maxlength="20"/>
							</label>
						</p>
						<p>
							<label>
								<strong>Discord ID</strong><br/>
								<input type="text" name="discord" style="width:280px; height:25px;" size="50" maxlength="50"/>
							</label>
						</p>
						<input type="checkbox" name="tos" value="tos1"/>He le&iacute;do y acepto los <a style="color:black;" target="_blank" href="tos.php"><b><u>T&eacute;rminos y condiciones de uso</u></b></a>
					</div>
					<a  class="btn btn-primary btn-sm pull-right" onClick="document.getElementById('login-form').submit()"><span>&nbsp;&nbsp;&nbsp;&nbsp;Enviar&nbsp;registro&nbsp;&nbsp;&nbsp;</span></a>
					<br clear="all" />
				</form>

				<div class="text-center box box-solid" style="margin-top: 5px;">
					<strong>Pintherol system (<span id="version">v0.0</span>)</strong> | <strong>Copyright &copy; 2016 <a href="#">Pintherol Team</a>.</strong> All rights reserved.
				</div>
			<?}?>
	  </div>
	</div>
	
	<script>
		function checkVersion()
		{
			$(document).ready(function()
			{
				$.post('./loaderproxy.php',{plugin:"acp", content:"ruta", version:''},
				function(output)
				{
					$('#version').html(output);
				});
			});
		}
	</script>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
  </body>
</html>
