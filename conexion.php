<?
	$dbhost = 'localhost';
	$db = 'pintherol';
	$securdb = $db.'_security';
	$dbuser = 'pintherol';
	$dbpass = 'asdf';

	class DB
	{
		private $_bd;
		
		public function __construct($_host, $_dbname, $_username, $_password)
		{
			try
			{
				$this->_bd = new PDO('mysql:host='.$_host.';dbname='.$_dbname, $_username, $_password);
			}
			catch(PDOException $e)
			{
				return $e->getMessage();
			}
		}
		
		public function getBD()
		{
			return $this->_bd;
		}
		
		public function select($query)
		{
			return $this->_bd->query($query);
		}
		
		public function update($query)
		{
			try
			{
				$stmnt = $this->_bd->prepare($query);
				if($stmnt->execute() == false)
				{
					return $stmnt->errorInfo()[2];
				}
			}
			catch(PDOException $e)
			{
				return $e->getMessage();
			}
		}
		
		public function insert($query)
		{
			try
			{
				$stmnt = $this->_bd->prepare($query);
				if($stmnt->execute() == false)
				{
					return $stmnt->errorInfo()[2];
				}
			}
			catch(PDOException $e)
			{
				return $e->getMessage();
			}
		}
		
		public function delete($query)
		{
			try
			{
				$stmnt = $this->_bd->prepare($query);
				if($stmnt->execute() == false)
				{
					return $stmnt->errorInfo()[2];
				}
			}
			catch(PDOException $e)
			{
				return $e->getMessage();
			}
		}
	}
	
	$bd = new DB($dbhost, $db, $dbuser, $dbpass);
?> 
