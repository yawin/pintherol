<?
	class FileManager
	{
		private $path = "./uploads/";
		
		public function __construct($plugin = "no_plugin")
		{
			$this->path = $this->path.$plugin.'/';
		}
		
		public function GetPath()
		{
			return $this->path;
		}
		
		public function CheckFolder()
		{
			return file_exists($this->path);
		}
		
		public function ListFolder($folder = "")
		{
			$pila = array();
			$ruta = $this->path.$folder;
			
			if(is_dir($ruta))
			{
				if($dh = opendir($ruta))
				{
					while(($file = readdir($dh)) !== false)
					{
						if(!is_dir($ruta.$file) && $file!="." && $file!="..")
						{
							array_push($pila,$file);
						}
					}
				}
				closedir($dh); 
			}
			
			return $pila;
		}
		
		public function Upload($file, $filename = "")
		{
			if($file==NULL) 
			{
				return;
			}

			$tamano=$file['size'];
			$tipo=$file['type'];
			
			if($filename == "")
			{
				$archivo=$file['name'];

				$dar_acena= str_replace("á","a",$archivo);
				$dar_acene= str_replace("é","e",$dar_acena);
				$dar_aceni= str_replace("í","i",$dar_acene);
				$dar_aceno= str_replace("ó","o",$dar_aceni);
				$dar_acenu= str_replace("ú","u",$dar_aceno);
				$dar_ene= str_replace("ñ","n",$dar_acenu);
				$dar_esp= str_replace(" ","_",$dar_ene);
				$nombre=$dar_esp;
			}
			else
			{
				$nombre = $filename;
			}

			$destino=$this->path.$nombre;
			$status=False;

			if(copy($file['tmp_name'],$destino)) 
			{
				$status=True;
			}
			
			return $status;
		}

		public function Delete($file)
		{
			if(file_exists($this->path.$file))
			{
				unlink($this->path.$file);
			}
		}
	}

	if(isset($_GET['func']))
	{
		if($_GET['func'] == 'load')
		{
			$fM = new FileManager($_GET['path']);
			if($fM->CheckFolder())
			{
				$filename = "";
				if(isset($_GET['filename'])){$filename = $_GET['filename'];}
				if($fM->Upload($_FILES["archivo"], $filename))
				{
					echo '1';
				}
			}
		}
	}
?>