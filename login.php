<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pintherol | Login</title>
	<link rel="shortcut icon" href="./img/d20pineado.png">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="./dist/css/skins/skin-blue-light.min.css">
  </head>
  <body class="hold-transition login-page" On OnLoad="evento(); checkVersion();">
	<div id="enerear" class="login-box">
	  <div class="login-logo">
			<a href="./index.php"><img style="width:75%;" src="./img/banner.png"/></a>
	  </div>
	  <!-- /.login-logo -->
	  <div class="login-box-body">
	    <p class="login-box-msg">Sign in to start your session</p>

	    <form>
	      <div class="form-group has-feedback">
	        <input autofocus type="text" id="username" class="form-control" placeholder="Username">
	        <span class="glyphicon glyphicon-user form-control-feedback"></span>
	      </div>
	      <div class="form-group has-feedback">
	        <input type="password" id="password" class="form-control" placeholder="Password">
	        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	      </div>
	      <div class="row">
	        <div class="col-xs-8">
			&nbsp;
	        </div>
	        <!-- /.col -->
	        <div class="col-xs-4">
	          <i class="btn btn-primary pull-right" OnClick="login();">Sign In</i>
	        </div>
	        <!-- /.col -->
	      </div>
	    </form>
		<div class="text-center box box-solid" style="margin-top: 5px;">
			<strong>Pintherol system (<span id="version">v0.0</span>)</strong> | <strong>Copyright &copy; 2016 <a href="#">Pintherol Team</a>.</strong> All rights reserved.
        </div>
	    <div class="row"><div class="text-center" id="login_info"></div></div> </div>
	  <!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->
	<script>
		function login()
		{
			username = document.getElementById("username").value;
			password = document.getElementById("password").value;

			$(document).ready(function()
			{
				$.post('./loaderproxy.php',{login:username, pass:password},
				function(output)
				{
					$('#login_info').html(output);
				});
			});
		}
		function checkVersion()
		{
			$(document).ready(function()
			{
				$.post('./loaderproxy.php',{plugin:"acp", content:"ruta", version:''},
				function(output)
				{
					$('#version').html(output);
				});
			});
		}

		function evento()
		{
			$(document).ready(function()
			{
				document.addEventListener("keypress", function(e)
				{
					var key = e.which || e.keyCode;
					if (key === 13)
					{
						login();
					}
				});
			});
		}
	</script>
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
  </body>
</html>
