<?
	require './cript.php';

	if(isset($_POST['orden']) && $priv > 100)
	{
		
		$bd_temp = new DB($dbhost, "pintherol_temp", $dbuser, $dbpass);
		$bd_old = new DB($dbhost, "pintherol_old", $dbuser, $dbpass);
		switch ($_POST['orden'])
		{
			case 'users_temp':
				$query = "CREATE TABLE IF NOT EXISTS `users_tmp` (
							`id` int(11) NOT NULL COMMENT 'Id de usuario',
							`username` varchar(25) NOT NULL COMMENT 'Nombre de usuario',
							`password` varchar(50) NOT NULL COMMENT 'Password del usuario',
							`discord` varchar(50) NOT NULL COMMENT 'Email del usuario',
							`privilegios` int(11) NOT NULL DEFAULT '0' COMMENT 'Privilegios del usuario',
							`old_id` int(11) NOT NULL COMMENT 'Old id de usuario'
						) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
						
						ALTER TABLE `users_tmp` ADD PRIMARY KEY (`id`);

						ALTER TABLE `users_tmp` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id de usuario',AUTO_INCREMENT=1;";
				$bd_temp->insert($query);

				$query = "TRUNCATE TABLE users_tmp";
				$bd_temp->insert($query);
				
				$query_old = "SELECT * FROM master";
				foreach($bd_old->select($query_old) as $r)
				{
					if($r['master'] != "Heathcliff")
					{
						$query = "INSERT INTO users_tmp (username, password, discord, privilegios, old_id) VALUES ('".$r['master']."', '".$r['pass']."', 0, 1,".$r['idmaster'].")";
						$bd_temp->insert($query);
					}
				}

				break;
			case 'partidas_old_temp':
				$query = "
					CREATE TABLE IF NOT EXISTS `partidas_old_tmp` (
						`idpartida` int(11) NOT NULL,
						`nombrepartida` varchar(40) DEFAULT NULL,
						`sistema` varchar(40) NOT NULL,
						`maxjugadores` int(11) NOT NULL,
						`dieciocho` varchar(2) NOT NULL,
						`noob` varchar(2) NOT NULL,
						`abierta` varchar(2) NOT NULL,
						`info` text NOT NULL,
						`idmaster` int(200) NOT NULL
					) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

					ALTER TABLE `partidas_old_tmp` ADD PRIMARY KEY (`idpartida`);

					ALTER TABLE `partidas_old_tmp` MODIFY `idpartida` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;";
				$bd_temp->insert($query);

				$query = "TRUNCATE TABLE partidas_old_tmp";
				$bd_temp->insert($query);
				
				$query_old = "SELECT * FROM partidas";
				foreach($bd_old->select($query_old) as $r)
				{
					$select = "SELECT id FROM users_tmp WHERE old_id = ".$r['idmaster'];
					foreach($bd_temp->select($select) as $sel)
					{
						$query = "INSERT INTO partidas_old_tmp (nombrepartida, sistema, maxjugadores, dieciocho, noob, abierta, info, idmaster) VALUES ('".$r['nombrepartida']."','".$r['sistema']."',".$r['maxjugadores'].",'".$r['dieciocho']."','".$r['noob']."','".$r['abierta']."','".$r['info']."',".$sel['id'].")";
					}

					$bd_temp->insert($query);
				}

				break;
			case 'partidas_temp':
				
				break;
			case 'registro_users':
				$select = "SELECT username, password, discord, privilegios FROM users_tmp";
				foreach($bd_temp->select($select) as $sel)
				{
					registra($sel['username'], $sel['password'], $sel['discord'], $sel['privilegios'],$bd);
				}
				break;
			case 'volcado_juegos':
				
				break;
			case 'volcado_partidas':
				
				break;
		}
	}

	function registra($username, $pass, $discord,$priv, $bd)
	{
		$cont = rand(9,20);
		$semilla = "";
		for($i=0;$i<$cont;$i++)
		{
			$semi=rand(0,9);
			$semilla=$semilla.$semi;
		}
		$contras=jarl($pass,$semilla);

		$GLOBALS['sesion']->set_login($username, $contras, $semilla);

		$query = 'INSERT INTO users (username,discord,privilegios) VALUES (\''.$username.'\',\''.$discord.'\','.$priv.')';
		$bd->insert($query);

		$query = 'SELECT id FROM users WHERE username=\''.$username.'\'';
		foreach($bd->select($query) as $row)
		{
			$cont=rand(9,20);
			$semilla="";
			for($i=0;$i<$cont;$i++)
			{
				$semi=rand(0,9);
				$semilla=$semilla.$semi;
			}

			$tok=jarl($semilla,$semilla);
			$query = 'INSERT INTO user_tokens (userid,token) VALUES ('.$row['id'].',\''.$tok.'\')';
			$bd->select($query); 
		}
	}
?>
