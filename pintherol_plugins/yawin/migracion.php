<?
	if($priv < 100)
	{
		return;
	}
?>
<section class="content">
	<div class="row">
		<div class="col-md-6"><div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Pasos para la migración</h3>
			</div>
			<div class="box-body">
				<div class="form-group box-body table-responsive no-padding">
					<ul class="timeline">
						<!-- timeline time label -->
						<li class="time-label">
							<span class="bg-green">
								Planificación
							</span>
						</li>
						<li>
							<i class="fa fa-check-square-o bg-green"></i>

							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>2017/06/25 01:00</span>

								<h3 class="timeline-header"><a href="#">Primero:</a> Analizar la base de datos vieja</h3>
							</div>
						</li>
						<li class="time-label">
							<span class="bg-yellow">
								Organización
							</span>
						</li>
						<li>
							<i class="fa fa-check-square-o bg-green"></i>

							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>2017/06/25 03:00</span>

								<h3 class="timeline-header no-border"><a href="#">Primero:</a> Generar tabla de usuarios/contraseñas/DiscordID/priv</h3>
								<div class="timeline-body">
									Cruzando los datos de la tabla masters y los participantes de las partidas, generar una tabla users_temp ("excluyendo a los dev") con los registros a introducir. A los users no registrados en masters, password aleatoria.
								</div>
								<div class="alert alert-danger alert-dismissible">
									<h4><i class="icon fa fa-ban"></i> Alert!</h4>
									No se puede automatizar la creación de cuentas de jugadores.
								</div>
								<div class="timeline-footer">
									<a class="btn btn-default btn-xs pull-right" OnClick="ejecuta('users_temp')">Ejecutar</a>
								</div>
							</div>
						</li>
						<li>
							<i class="fa fa-check-square-o bg-green"></i>

							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>2017/06/25 03:30</span>

								<h3 class="timeline-header no-border"><a href="#">Segundo:</a> Generar tabla de partidas_old_temp</h3>
								<div class="timeline-body">
									Tunear las partidas de la tabla vieja de partidas para unificar nombres de juegos de rol. Las que haya dudas, id de juego = 0 y ya se editará a mano.
								</div>
								<div class="alert alert-warning alert-dismissible">
									<h4><i class="icon fa fa-warning"></i> Warning!</h4>
									La unificación es manual.
								</div>
								<div class="timeline-footer">
									<a class="btn btn-default btn-xs pull-right" OnClick="ejecuta('partidas_old_temp')">Ejecutar</a>
								</div>
							</div>
						</li>
						<li>
							<i class="fa fa-check-square-o bg-green"></i>

							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>2017/06/25 03:30</span>

								<h3 class="timeline-header no-border"><a href="#">Tercero:</a> Generar tabla de juegos_temp</h3>
								<div class="timeline-body">
									Registrar los juegos de la lista anterior en una tabla temporal.
								</div>
								<div class="alert alert-success alert-dismissible">
									<h4><i class="icon fa fa-check"></i> Success!</h4>
									Al final no ha hecho falta porque se han introducido a mano.
								</div>
							</div>
						</li>
						<li>
							<i class="fa fa-pencil-square-o bg-yellow"></i>

							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>pendiente</span>

								<h3 class="timeline-header no-border"><a href="#">Cuarto:</a> Generar tabla de partidas_temp y de users_en_partida_temp</h3>
								<div class="timeline-body">
									Extraer datos de partidas y cruzarlas con users_temp.
								</div>
								<div class="timeline-footer">
									<a class="btn btn-default btn-xs pull-right" OnClick="ejecuta('partidas_temp')">Ejecutar</a>
								</div>
							</div>
						</li>
						<li class="time-label">
							<span class="bg-red">
								Ejecución
							</span>
						</li>
						<li>
							<i class="fa fa-pencil-square-o bg-red"></i>

							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>pendiente</span>

								<h3 class="timeline-header no-border"><a href="#">Primero:</a> Registrar usuarios</h3>
								<div class="timeline-body">
									Iteración de users_temp llamando a la función de registro.
								</div>
								<div class="timeline-footer">
									<a class="btn btn-default btn-xs pull-right" OnClick="ejecuta('registro_users')">Ejecutar</a>
								</div>
							</div>
						</li>
						<li>
							<i class="fa fa-check-square-o bg-green"></i>

							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>pendiente</span>

								<h3 class="timeline-header no-border"><a href="#">Segundo:</a> Registrar juegos</h3>
								<div class="timeline-body">
									Volcar juegos_temp en juegos y modificar partidas_temp con los nuevos índices de los juegos.
								</div>
								<div class="alert alert-success alert-dismissible">
									<h4><i class="icon fa fa-check"></i> Success!</h4>
									Al final no hace falta porque se han introducido a mano.
								</div>
								<div class="timeline-footer">
									<a class="btn btn-default btn-xs pull-right" OnClick="ejecuta('volcado_juegos')">Ejecutar</a>
								</div>
							</div>
						</li>
						<li>
							<i class="fa fa-pencil-square-o bg-red"></i>

							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>pendiente</span>

								<h3 class="timeline-header no-border"><a href="#">Tercero:</a> Registrar partidas</h3>
								<div class="timeline-body">
									Volcar partidas_temp en partidas y users_en_partida_temp en users_en_partida.
								</div>
								<div class="timeline-footer">
									<a class="btn btn-default btn-xs pull-right" OnClick="ejecuta('volcado_partidas')">Ejecutar</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div></div>
	</div>
</section>
<script>
	function ejecuta(orden)
	{
		pagina = "utils";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:orden},
			function(output)
			{
				$("#debug").html(output);
			});
		});
	}
	/*function recarga()
	{
		pagina = "juegos";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin},
			function(output)
			{
				$('#contenido').html(output);
			});
		});
	}
	function saveNew()
	{
		value = $("#system_0").val();
		table = "juegos";
		_saveData("newdata", table, value, 0);
	}
	function updateData(field,id)
	{
		value = $(field).val();
		table = "juegos";
		_saveData("updatedata", table, value, id);
	}
	function updateSistema(field,id)
	{
		value = $(field).val();
		table = "juegos";
		_saveData("updatesistema", table, value, id);
	}
	function _saveData(orden, table, value, id)
	{
		pagina = "utils";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:orden, table:table, value:value, id:id},
			function(output)
			{
 				recarga();
			});
		});
	}*/
</script>
