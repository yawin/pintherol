<section class="content">
	<div class="row">
		<div class="col-md-12"><div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Partidas abiertas</h3>
			</div>
			<div class="box-body">
				<div class="form-group box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th style="width: 10px">#</th>
								<th>Nombre</th>
								<th>Juego</th>
								<th>Sistema</th>
								<th style="width: 100px">Master</th>
								<th style="width: 100px">Max. Jug.</th>
								<th>+18</th>
								<th>Noobfriendly</th>
								<th></th>
							</tr>
							<?
								$query = "SELECT * FROM partidas WHERE abierta = 1";
								foreach($bd->select($query) as $r)
								{?>
									<tr>
										<td><?echo $r['id']?></td>
										<td><?echo $r['nombre'];?></td>
										<td><?
											$cont = 0;
											$query = "SELECT nombre FROM juegos WHERE id = ".$r['juego'];
											foreach($bd->select($query) as $row)
											{
												echo $row[0];
											}?>
										</td>
										<td><?
											$cont = 0;
											$query = "SELECT s.nombre FROM sistemas AS s INNER JOIN juegos AS j ON j.id = ".$r['juego']." AND s.id = j.sistema";
											foreach($bd->select($query) as $row)
											{
												echo $row[0];
											}?>
										</td>
										<td><?
											$cont = 0;
											$query = "SELECT username FROM users WHERE id = ".$r['master'];
											foreach($bd->select($query) as $row)
											{
												echo $row[0];
											}?>
										</td>
										<td><? $query = "SELECT COUNT(userid) as cont FROM users_en_partida WHERE partidaid = ".$r['id']." AND aceptado = 1";
											foreach($bd->select($query) as $jug)
											{
												echo $jug['cont']."/".$r['max_jugadores'];
											}?>
										</td>
										<td><?
											if($r['pegi'] == 1){ echo 'Sí';}else{ echo 'No';}?>
										</td>
										<td><?
											if($r['noobsfriendly'] == 1){ echo 'Sí';}else{ echo 'No';}?>
										</td>
										<td id="partida_<?echo $r['id'];?>">
										<? if(isset($userid) and $userid > 0 and $r['master'] != $userid){
											$query = "SELECT count(userid) FROM users_en_partida WHERE partidaid = ".$r['id']." AND userid = ".$userid;
											foreach($bd->select($query) as $row)
											{
												if($row[0] == 0)
												{?>
													<button class="btn btn-default btn-sm" data-widget="" data-toggle="tooltip" title="" data-original-title="Unirse a la partida" OnClick="join(<? echo $r['id'];?>);"><i class="glyphicon glyphicon-plus"></i></button>
												<?}
												else
												{
													$query = "SELECT aceptado FROM users_en_partida WHERE partidaid = ".$r['id']." AND userid = ".$userid;
													foreach($bd->select($query) as $row)
													{
														if($row['aceptado'] == 0)
														{?>
															<button class="btn btn-default btn-sm" data-widget="" data-toggle="tooltip" title="" data-original-title="Salirse de la partida" OnClick="unjoin(<? echo $r['id'];?>);"><i class="glyphicon glyphicon-minus"></i></button>
														<?}
														else
														{?>
															<button class="btn btn-success btn-sm" data-widget="" data-toggle="tooltip" title="" data-original-title="¡Has sido aceptado!" OnClick=""><i class="glyphicon glyphicon-ok"></i></button>
														<?}
													}
												}
											}
										}?>
										<button id="btn-ver-mas-<? echo $r['id'];?>" class="btn btn-default btn-xs pull-right" data-widget="" data-toggle="tooltip" title="" data-original-title="Ver más" OnClick="$('#tr-part-<? echo $r['id'];?>').removeClass('hidden'); $('#btn-ver-menos-<? echo $r['id'];?>').removeClass('hidden'); $('#btn-ver-mas-<? echo $r['id'];?>').addClass('hidden');">Ver más</button>
										<button id="btn-ver-menos-<? echo $r['id'];?>" class="btn btn-default btn-xs pull-right hidden" data-widget="" data-toggle="tooltip" title="" data-original-title="Ocultar" OnClick="$('#tr-part-<? echo $r['id'];?>').addClass('hidden'); $('#btn-ver-menos-<? echo $r['id'];?>').addClass('hidden'); $('#btn-ver-mas-<? echo $r['id'];?>').removeClass('hidden');">Ocultar</button>
										</td>
									</tr>
									<tr id="tr-part-<? echo $r['id'];?>" class="hidden">
										<td></td>
										<td><b>Sinopsis:</b> <? echo $r['sinopsis']; if($r['sinopsis'] == ""){?>No se ha introducido una sinopsis<?}?></td>
									</tr>
								<?}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div></div>
	</div>
</section>
<script>
	function join(partidaid)
	{
		plugin = "<?echo $plugin;?>";
		pagina = "utils";
		orden = "unirse"
	
		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:orden, partida:partidaid},
			function(output)
			{
				loadPage('index','inicio');
			});
		});
	}
	function unjoin(partidaid)
	{
		plugin = "<?echo $plugin;?>";
		pagina = "utils";
		orden = "desunirse"
	
		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:orden, partida:partidaid},
			function(output)
			{
				loadPage('index','inicio');
			});
		});
	}
</script>
