<?
	if(isset($_POST['partida']))
	{
		$query = "SELECT * FROM partidas WHERE id = ".$_POST['partida'];
		foreach($bd->select($query) as $r)
		{
			if($r['master'] == $userid)
			{
				editable($r,$bd);
			}
			else
			{
				visor($r,$bd);
			}?>
			<script>
				function cargaFiles()
				{
					pagina = "files";

					$(document).ready(function()
					{
						$.post('./loaderproxy.php',{content:pagina, plugin:plugin, partida:<? echo $r['id'];?>},
						function(output)
						{
							$("#files").html(output);
						});
					});
				}
				
				cargaFiles();
			</script>
		<?}
	}
	
	function editable($r,$bd)
	{?>
		<section class="content">
			<div class="row">
				<div class="col-md-8">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title"><? echo $r['nombre'];?></h3>
							<button class="btn btn-danger btn-xs pull-right" data-widget="" data-toggle="tooltip" title="" data-original-title="Eliminar partida" OnClick="elimina();">Eliminar partida</button>
						</div>
						<div class="box-body">
						<table class="table no-border">
								<tbody>
									<tr>
										<th style="width: 300px;">
											Nombre de la partida
										</th>
										<th style="width: 100px;">
											Max. Jug.
										</th>
										<th>
											Juego
										</th>
									</tr>
									<tr>
										<td><input OnChange="saveData('nombre','#newform_nombre');" style="width:100%;" id="newform_nombre" type="text" value="<? echo $r['nombre'];?>"/></td>
										<td><input OnChange="saveData('max_jugadores','#newform_max_jugadores');"style="width:100%;" id="newform_max_jugadores" type="text" value="<? echo $r['max_jugadores'];?>"/></td>
										<td>
											<select OnChange="saveData('juego','#newform_juego');" id="newform_juego" class="form-control">
												<? $query = "SELECT * FROM juegos WHERE id > 0";
												foreach($bd->select($query) as $r2)
												{?>
													<option value="<? echo $r2['id'];?>" <? if($r2['id']==$r['juego']){ echo "selected";}?>><? echo $r2['nombre'];?></option>
												<?}?>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
							<table class="table no-border">
								<tbody>
									<tr>
										<th style="width: 400px;">Sinopsis</th>
									</tr>
									<tr>
										<td>
											<input OnChange="saveData('sinopsis','#newform_sinopsis');" style="width:100%;" id="newform_sinopsis" type="text" value="<? echo $r['sinopsis'];?>"/>
										</td>
										<td>
											<span class="pull-right">
												Noobfriendly <input <? if($r['noobsfriendly']==1){ echo "checked";}?> OnChange="saveCheck('noobsfriendly','#newform_noobs');" id="newform_noobs" type="checkbox"/>
												&nbsp;
												+18 <input <? if($r['pegi']==1){ echo "checked";}?> OnChange="saveCheck('pegi','#newform_pegi');" id="newform_pegi" type="checkbox"/>
											</span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div id="files" class="box"></div>
				</div>
				<div class="col-md-4">
					<?  $query = "SELECT COUNT(userid) as cont FROM users_en_partida WHERE partidaid = ".$r['id']." AND aceptado = 1";
						foreach($bd->select($query) as $jug)
						{?>
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Jugadores admitidos (<? echo $jug['cont'];?>/<? echo $r['max_jugadores'];?>)</h3>
								</div>
								<div class="box-body">
									<? if($jug['cont'] > 0)
									{?>
										<table class="table table-hover no-border">
											<tbody>
												<tr>
													<th style="width: 10px;">#</th>
													<th style="width: 100px;">Nombre</th>
													<th style="width: 100px;">Email</th>
												</tr>
												<? $query = "SELECT id, username, discord FROM users AS u INNER JOIN users_en_partida AS uep ON u.id = uep.userid AND uep.partidaid = ".$r['id']." AND aceptado = 1";
												foreach($bd->select($query) as $j)
												{?>
													<tr class="clickable-des" data-href="<? echo $j['id'];?>">
														<td><? echo $j['id'];?></td>
														<td><? echo $j['username'];?></td>
														<td><? echo $j['discord'];?></td>
													</tr>
												<?}?>
											</tbody>
										</table>
									<?}
									else
									{?>
										No hay jugadores admitidos.
									<?}?>
								</div>
							</div>
					<?  } 

						$query = "SELECT COUNT(userid) as cont FROM users_en_partida WHERE partidaid = ".$r['id']." AND aceptado = 0";
						foreach($bd->select($query) as $jug)
						{?>
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Jugadores sin admitir (<? echo $jug['cont'];?>)</h3>
								</div>
								<div class="box-body">
									<? if($jug['cont'] > 0)
									{?>
										<table class="table table-hover no-border">
											<tbody>
												<tr>
													<th style="width: 10px;">#</th>
													<th style="width: 100px;">Nombre</th>
													<th style="width: 100px;">Email</th>
												</tr>
												<?
												$query = "SELECT id, username, discord FROM users AS u INNER JOIN users_en_partida AS uep ON u.id = uep.userid AND uep.partidaid = ".$r['id']." AND uep.aceptado = 0";
												foreach($bd->select($query) as $j)
												{?>
													<tr class="clickable-adm" data-href="<? echo $j['id'];?>">
														<td><? echo $j['id'];?></td>
														<td><? echo $j['username'];?></td>
														<td><? echo $j['discord'];?></td>
													</tr>
												<?}?>
											</tbody>
										</table>
									<?}
									else
									{?>
										No hay jugadores sin admitir.
									<?}?>
								</div>
							</div>
					<?  }?>
				</div>
			</div>
		</section>
		<script>
			function recarga()
			{
				pagina = "editar";

				$(document).ready(function()
				{
					$.post('./loaderproxy.php',{content:pagina, plugin:plugin, partida:<? echo $r['id'];?>},
					function(output)
					{
						$('#contenido').html(output);
					});
				});
			}

			function saveCheck(id,field)
			{		
				value = 0;
				if($(field).prop("checked") == true)
				{
					value = 1;
				}
				
				_saveData(id, value);
			}
			function saveData(id,field)
			{		
				value = $(field).val();
				_saveData(id, value);
			}
			function _saveData(id, value)
			{
				pagina = "utils";

				$(document).ready(function()
				{
					$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"guardar", partida:<? echo $r['id'];?>, field:id, value:value},
					function(output)
					{
 						recarga();
					});
				});
			}
			
			function elimina()
			{
				pagina = "utils";

				$(document).ready(function()
				{
					$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"borrar", partida:<? echo $r['id'];?>},
					function(output)
					{
						loadPage("list","mis-partidas");
					});
				});
			}
			
			function admite(userid)
			{
				pagina = "utils";

				$(document).ready(function()
				{
					$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"acepta", partida:<? echo $r['id'];?>, userid:userid},
					function(output)
					{
 						recarga();
					});
				});
			}
			
			function desadmite(userid)
			{
				pagina = "utils";

				$(document).ready(function()
				{
					$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"desacepta", partida:<? echo $r['id'];?>, userid:userid},
					function(output)
					{
 						recarga();
					});
				});
			}

			jQuery(document).ready(function($)
			{
				$(".clickable-des").click(function()
				{
					desadmite($(this).data("href"));
				});
				$(".clickable-adm").click(function()
				{
					admite($(this).data("href"));
				});
			});
		</script>
	<?}
	
	function visor($r,$bd)
	{?>
		<section class="content">
			<div class="row">
				<div class="col-md-8">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title"><? echo $r['nombre'];?></h3>
						</div>
						<div class="box-body">
						<table class="table no-border">
								<tbody>
									<tr>
										<th style="width: 300px;">
											Nombre de la partida
										</th>
										<th style="width: 100px;">
											Max. Jug.
										</th>
										<th>
											Juego
										</th>
									</tr>
									<tr>
										<td><? echo $r['nombre'];?></td>
										<td><? echo $r['max_jugadores'];?></td>
										<td>
											<? $query = "SELECT nombre FROM juegos WHERE id = ".$r['juego'];
												foreach($bd->select($query) as $r2)
												{
													echo $r2['nombre'];
												}
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table class="table no-border">
								<tbody>
									<tr>
										<th style="width: 400px;">Sinopsis</th>
									</tr>
									<tr>
										<td>
											<? echo $r['sinopsis'];?>
										</td>
										<td>
											<b>Noobfriendly:</b> <? if($r['noobsfriendly']==1){ echo "Sí";}else{ echo "No";}?>&nbsp;
										</td>
										<td>
											<b>+18:</b> <? if($r['pegi']==1){ echo "Sí";}else{ echo "No";}?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div id="files" class="box"></div>
				</div>
				<div class="col-md-4">
					<?  $query = "SELECT COUNT(userid) as cont FROM users_en_partida WHERE partidaid = ".$r['id']." AND aceptado = 1";
						foreach($bd->select($query) as $jug)
						{?>
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title">Jugadores (<? echo $jug['cont'];?>/<? echo $r['max_jugadores'];?>)</h3>
								</div>
								<div class="box-body">
									<? if($jug['cont'] > 0)
									{?>
										<table class="table table-hover no-border">
											<tbody>
												<tr>
													<th style="width: 10px;">#</th>
													<th style="width: 100px;">Nombre</th>
													<th style="width: 100px;">Email</th>
												</tr>
												<? $query = "SELECT id, username, discord FROM users AS u INNER JOIN users_en_partida AS uep ON u.id = uep.userid AND uep.partidaid = ".$r['id']." AND aceptado = 1";
												foreach($bd->select($query) as $j)
												{?>
													<tr>
														<td><? echo $j['id'];?></td>
														<td><? echo $j['username'];?></td>
														<td><? echo $j['discord'];?></td>
													</tr>
												<?}?>
											</tbody>
										</table>
									<?}
									else
									{?>
										No hay jugadores admitidos.
									<?}?>
								</div>
							</div>
					<?  }?>
				</div>
			</div>
		</section>
	<?}
?>
