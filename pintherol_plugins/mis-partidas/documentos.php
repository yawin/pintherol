<?
	if(!isset($_POST['partida'])){echo "No se ha especificado partida";}
	$partida = $_POST['partida'];
	$master = 0;
	$query = "SELECT * FROM partidas WHERE id = ".$partida;
	foreach($bd->select($query) as $r)
	{
		if($r['master'] == $userid)
		{
			$master = 1;
		}
	}
	
	if($master == 0)
	{
		$query = "SELECT aceptado FROM users_en_partida WHERE partidaid = ".$partida." AND userid = ".$userid;
		foreach($bd->select($query) as $r)
		{
			if($r['aceptado'] == 0)
			{?>
				<script>
					$('#files').addClass('hidden');
				</script>
				<?return;
			}
		}
	}
?>
<div class="box-header with-border">
	<h3 class="box-title">Documentos</h3>
	
	<? if($master == 1)
	{?>
		<button id="btn-mos" class="btn btn-default btn-xs pull-right" data-widget="" data-toggle="tooltip" title="" data-original-title="Añadir documento" OnClick="$('#newform').removeClass('hidden'); $('#btn-hid').removeClass('hidden'); $('#btn-mos').addClass('hidden');">Añadir documento</button>
		<button id="btn-hid" class="btn btn-default btn-xs pull-right hidden" data-widget="" data-toggle="tooltip" title="" data-original-title="Ocultar" OnClick="$('#newform').addClass('hidden'); $('#btn-hid').addClass('hidden'); $('#btn-mos').removeClass('hidden');">Ocultar</button>
	<?}?>
</div>
<div class="box-body">
	<? if($master == 1)
	{?>
		<div id="newform" class="hidden row">
			<table class="table table-hover no-border">
				<tbody>
					<tr>
						<th></th>
						<th>Nombre:</th>
						<td><input  style="width: 100%;" onchange="" name="nombre" id="filename"></td>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<th></th>
						<th>Fichero:</th>
						<td>
							<form enctype="multipart/form-data" class="form-group col-md-12" id="cargador">
								<input onchange="" name="archivo" id="cargafile" type="file">
							</form>
						</td>
						<td>
							<button class="btn btn-default" data-widget="" data-toggle="tooltip" title="" data-original-title="Subir" OnClick="subeFile();">Subir</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?}?>
	<div>
		<? $query = "SELECT COUNT(id) FROM files WHERE partida = ".$partida;
		foreach($bd->select($query) as $cont)
		{ 
			if($cont[0] > 0)
			{?>
				<table class="table table-hover no-border">
					<tbody>
						<tr>
							<th>Nombre</th>
							<th style="width: 50px;">Fichero</th>
							<th></th>
						</tr>
						<? $query = "SELECT * FROM files WHERE partida = ".$partida;
						foreach($bd->select($query) as $r)
						{?>
							<tr>
								<td><? echo $r['nombre'];?></td>
								<td><? echo $r['url'];?></td>
								<td>
									<span class="pull-right">
										<a target="_blank" title="Descargar" href="./uploads/<? echo $plugin;?>/<? echo $r['url'];?>"><i class="glyphicon glyphicon-cloud-download"></i></a>&nbsp;
										<?if($master == 1)
										{?>
											<a OnClick="eliminaFile(<?echo $r['id'];?>)" title="Eliminar"><i class="glyphicon glyphicon-remove-sign"></i></a>
										<?}?>
									</span>
								</td>
							</tr>
						<?}?>
					</tbody>
				</table>
			<?}
			else
			{?>
				No hay documentos disponibles.
			<?}
		}
		
		if($master == 1)
		{?>
			<script>
				function eliminaFile(fileid)
				{
					pagina = "utils";
					
					$(document).ready(function()
					{
						$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"delfile", partida:<? echo $partida;?>, fileid:fileid},
						function(output)
						{
							$("#debug").html(output);
							cargaFiles();
						});
					});
				}

				function subeFile()
				{
					path = "<? echo $plugin;?>";
					$(document).ready(function()
					{
						var formData = new FormData($("#cargador")[0]);
						
						var fileName;
						$("input[type='file']").each(function() {
							filename = $(this).val().split('/').pop().split('\\').pop();
						});
						filename = '<? echo $partida;?>_'+filename;
						
						$.ajax(
						{
							url: 'utils.php?func=load&path='+path+'&filename='+filename,
							type: 'POST',
							data: formData,
							async: false,
							success: function(data) 
							{
								if(data == '1')
								{
									pagina = "utils";

									$(document).ready(function()
									{
										$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"newfile", partida:<? echo $partida;?>, nombre:$("#filename").val(), url:filename},
										function(output)
										{
											cargaFiles();
										});
									});
								}
							},
							cache: false,
							contentType: false,
							processData: false
						});
					});
				}
			</script>
		<?}?>
	</div>
</div>
