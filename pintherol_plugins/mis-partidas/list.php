<section class="content">
	<div class="row">
		<? if($priv > 0)
		{?>
			<div class="col-md-12"><div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Partidas que diriges</h3>
					<button id="btn-mos" class="btn btn-default btn-xs pull-right" data-widget="" data-toggle="tooltip" title="" data-original-title="Nueva partida" OnClick="$('#newform').removeClass('hidden'); $('#btn-hid').removeClass('hidden'); $('#btn-mos').addClass('hidden');">Nueva</button>
					<button id="btn-hid" class="btn btn-default btn-xs pull-right hidden" data-widget="" data-toggle="tooltip" title="" data-original-title="Ocultar" OnClick="$('#newform').addClass('hidden'); $('#btn-hid').addClass('hidden'); $('#btn-mos').removeClass('hidden');">Ocultar</button>
				</div>
				<div class="box-body">
					<div id="newform" class="hidden">
						<table class="table no-border">
							<tbody>
								<tr>
									<th style="width: 300px;">
										Nombre de la partida
									</th>
									<th style="width: 200px;">
										Max. Jugadores
									</th>
									<th>
										Juego
									</th>
								</tr>
								<tr>
									<td><input style="width:100%;" id="newform_nombre" type="text" value=""/></td>
									<td><input style="width:100%;" id="newform_max_jugadores" type="text" value=""/></td>
									<td>
										<select OnChange="" id="newform_juego" class="form-control">
											<? $query = "SELECT * FROM juegos ORDER BY nombre";
											foreach($bd->select($query) as $r)
											{?>
												<option value="<? echo $r['id'];?>"><? echo $r['nombre'];?></option>
											<?}?>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="table no-border">
							<tbody>
								<tr>
									<th style="width: 500px;">Sinopsis</th>
								</tr>
								<tr>
									<td>
										<input style="width:100%;" id="newform_sinopsis" type="text" value=""/>
									</td>
									<td>
										<span class="pull-right">
											Noobfriendly <input OnChange="" id="newform_noobs" type="checkbox"/>
											&nbsp;
											+18 <input OnChange="" id="newform_pegi" type="checkbox"/>
										</span>
									</td>
									<td style="width: 100px;">
										<button class="btn btn-success btn-xs pull-right" data-widget="" data-toggle="tooltip" title="" data-original-title="Crear partida" OnClick="crearpartida();">Crear</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="form-group box-body table-responsive no-padding">
						<? $query = "SELECT count(id) FROM partidas WHERE master = ".$userid;
						foreach($bd->select($query) as $r)
						{
							if($r[0] > 0)
							{?>
								<table class="table table-hover">
									<tbody>
										<tr>
											<th style="width: 10px">#</th>
											<th>Nombre</th>
											<th>Juego</th>
											<th>Sistema</th>
											<th style="width: 100px">Max. Jugadores</th>
											<th>+18</th>
											<th>Noobfriendly</th>
											<th></th>
										</tr>
										<?
											$query = "SELECT * FROM partidas WHERE master = ".$userid;
											foreach($bd->select($query) as $r)
											{?>
												<tr>
													<td><?echo $r['id']?></td>
													<td><?echo $r['nombre'];?></td>
													<td><?
														$cont = 0;
														$query = "SELECT nombre FROM juegos WHERE id = ".$r['juego'];
														foreach($bd->select($query) as $row)
														{
															echo $row[0];
														}?>
													</td>
													<td><?
														$cont = 0;
														$query = "SELECT s.nombre FROM sistemas AS s INNER JOIN juegos AS j ON j.id = ".$r['juego']." AND s.id = j.sistema";
														foreach($bd->select($query) as $row)
														{
															echo $row[0];
														}?>
													</td>
													<td>
														<? $query = "SELECT COUNT(userid) as cont FROM users_en_partida WHERE partidaid = ".$r['id']." AND aceptado = 1";
														foreach($bd->select($query) as $jug)
														{
															echo $jug['cont']."/".$r['max_jugadores'];
														}?>
													</td>
													<td><?
														if($r['pegi'] == 1){ echo 'Sí';}else{ echo 'No';}?>
													</td>
													<td><?
														if($r['noobsfriendly'] == 1){ echo 'Sí';}else{ echo 'No';}?>
													</td>
													<td>
														<button class="btn btn-default btn-sm" data-widget="" data-toggle="tooltip" title="" data-original-title="Editar partida" OnClick="edita(<?echo $r['id']?>)"><i class="glyphicon glyphicon-eye-open"></i></button>

														<? if($r['abierta'] == 0)
														{?>
															<button class="btn btn-default btn-sm" data-widget="" data-toggle="tooltip" title="" data-original-title="Partida cerrada" OnClick="abrir_partida(<? echo $r['id'];?>);"><i class="glyphicon glyphicon-lock"></i></button>
														<?}
														else
														{?>
															<button class="btn btn-default btn-sm" data-widget="" data-toggle="tooltip" title="" data-original-title="Partida abierta" OnClick="cerrar_partida(<? echo $r['id'];?>);"><i class="glyphicon glyphicon-folder-open"></i></button>
														<?}?>
													</td>
												</tr>
											<?}
										?>
									</tbody>
								</table>
							<?}
							else
							{?>
								No mastereas ninguna partida.
							<?}
						}?>
					</div>
				</div>
			</div></div>
		<?}?>
		<div class="col-md-12"><div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Partidas en las que estás apuntado</h3>
			</div>
			<div class="box-body">
				<div class="form-group box-body table-responsive no-padding">
					<?	$query = "SELECT count(userid) FROM users_en_partida WHERE userid = ".$userid;
						foreach($bd->select($query) as $r)
						{
							if($r[0] > 0)
							{?>
								<table class="table table-hover">
									<tbody>
									<tr>
										<th style="width: 10px">#</th>
										<th>Nombre</th>
										<th>Juego</th>
										<th style="width: 100px">Master</th>
										<th style="width: 100px">Max. Jugadores</th>
										<th>+18</th>
										<th>Noobfriendly</th>
										<th></th>
									</tr>
										<?
											$query="SELECT DISTINCT id, nombre, juego, master, max_jugadores, pegi, noobsfriendly, abierta, sinopsis FROM partidas AS p INNER JOIN users_en_partida AS u ON u.partidaid = p.id AND u.userid = ".$userid;
											foreach($bd->select($query) as $r)
											{?>
												<tr>
													<td><?echo $r['id']?></td>
													<td><?echo $r['nombre'];?></td>
													<td><?
														$cont = 0;
														$query = "SELECT nombre FROM juegos WHERE id = ".$r['juego'];
														foreach($bd->select($query) as $row)
														{
															echo $row[0];
														}?>
													</td>
													<td><?
														$cont = 0;
														$query = "SELECT username FROM users WHERE id = ".$r['master'];
														foreach($bd->select($query) as $row)
														{
															echo $row[0];
														}?>
													</td>
													<td><?echo $r['max_jugadores'];?></td>
													<td><?
														if($r['pegi'] == 1){ echo 'Sí';}else{ echo 'No';}?>
													</td>
													<td><?
														if($r['noobsfriendly'] == 1){ echo 'Sí';}else{ echo 'No';}?>
													</td>
													<td>
													<? if(isset($userid) and $userid > 0)
													{
														$query = "SELECT count(userid) FROM users_en_partida WHERE partidaid = ".$r['id']." AND userid = ".$userid;
														foreach($bd->select($query) as $row)
														{
															if($row[0] > 0)
															{
																$query = "SELECT aceptado FROM users_en_partida WHERE partidaid = ".$r['id']." AND userid = ".$userid;
																foreach($bd->select($query) as $row)
																{
																	if($row['aceptado'] == 0)
																	{?>
																		<button class="btn btn-default btn-sm" data-widget="" data-toggle="tooltip" title="" data-original-title="Salirse de la partida" OnClick="unjoin_alt(<? echo $r['id'];?>);"><i class="glyphicon glyphicon-minus"></i></button>
																	<?}
																	else
																	{?>
																		<button class="btn btn-success btn-sm" data-widget="" data-toggle="tooltip" title="" data-original-title="¡Has sido aceptado!" OnClick=""><i class="glyphicon glyphicon-ok"></i></button>
																	<?}
																}
															}
														}
													}?>
													<button class="btn btn-default btn-sm" data-widget="" data-toggle="tooltip" title="" data-original-title="Ver partida" OnClick="edita(<?echo $r['id']?>)"><i class="glyphicon glyphicon-eye-open"></i></button>
													
													<? if($r['abierta'] == 0)
													{?>
														<button class="btn btn-default btn-sm" data-widget="" data-toggle="tooltip" title="" data-original-title="Partida cerrada" OnClick=""><i class="glyphicon glyphicon-lock"></i></button>
													<?}?>
													</td>
												</tr>
											<?}
										?>
									</tbody>
								</table>
							<?}
							else
							{?>
								No estás apuntado a ninguna partida.
							<?}
						}?>
				</div>
			</div>
		</div></div>
	</div>
</section>
<script>
	function unjoin_alt(partidaid)
	{
		plugin = "inicio";
		pagina = "utils";
		orden = "desunirse"
	
		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:orden, partida:partidaid},
			function(output)
			{
				loadPage('list','mis-partidas');
			});
		});
	}
	function abrir_partida(partidaid)
	{
		pagina = "utils";
		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, partida:partidaid, orden:"abre"},
			function(output)
			{
				loadPage("list","mis-partidas");
			});
		});
	}
	function cerrar_partida(partidaid)
	{
		pagina = "utils";
		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, partida:partidaid, orden:"cierra"},
			function(output)
			{
				loadPage("list","mis-partidas");
			});
		});
	}
	
	function edita(partidaid)
	{
		pagina = "editar";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, partida:partidaid},
			function(output)
			{
				$('#contenido').html(output);
			});
		});
	}
	
	function crearpartida()
	{
		plugin = "mis-partidas";
		pagina = "utils";
		orden = "crear";
		
		nombre = $("#newform_nombre").val();
		max_jugadores = $("#newform_max_jugadores").val();
		juego = $("#newform_juego").val();
		noobs = 0;
		if($("#newform_noobs").val() == "on"){noobs = 1;}
		pegi = 0;
		if($("#newform_pegi").val() == "on"){pegi = 1;}
		sinopsis = $("#newform_sinopsis").val();
	
		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:orden, nombre:nombre, max_jugadores:max_jugadores, juego:juego, noobs:noobs, pegi:pegi, sinopsis:sinopsis},
			function(output)
			{
				loadPage('list', 'mis-partidas');
			});
		});
	}
</script>
