<? if(!isset($_POST['orden']))
{?>
<section class="content">
	<div class="row">
		<div class="col-md-6"><div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Usuarios</h3>
			</div>
			<div id="usertable" class="box-body">
			</div>
		</div></div>
	</div>
</section>
<script>
	function reload()
	{
		pagina = "ucp";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"cargaUser"},
			function(output)
			{
				$("#usertable").html(output);
			});
		});
	}
	
	function _saveData()
	{
		value = $("#user_<? echo $userid;?>_discord").val();
		pagina = "ucp";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"savedata", value:value},
			function(output)
			{
				reload();
			});
		});
	}
	
	reload();
</script>
<?}
else
{
	switch($_POST['orden'])
	{
			case "cargaUser":?>
				<table class="table table-hover">
					<tbody>
						<tr>
							<th style="width: 10px">#</th>
							<th>Nombre</th>
							<th>Discord ID</th>
						</tr>
						<?
							$query = "SELECT * FROM users WHERE id = ".$userid;
							foreach($bd->select($query) as $r)
							{?>
								<tr>
									<td><? echo $userid;?></td>
									<td><? echo $r['username'];?></td>
									<td><input OnChange="_saveData();" style="width:100%;" id="user_<? echo $userid;?>_discord" value="<? echo $r['discord'];?>"></td>
								</tr>
							<?}
						?>
					</tbody>
				</table>
				<table class="table table-hover">
					<tbody>
						<tr>
							<th>Cambiar la contraseña</th>
							<th>Rango</th>
						</tr>
						<?
							$query = "SELECT * FROM users WHERE id = ".$userid;
							foreach($bd->select($query) as $r)
							{?>
								<tr>
									<td><input type="password" OnChange="cambiaPass();" style="width:100%;" id="user_<? echo $userid;?>_pass" placeholder="Introduce la nueva contraseña"></td>
									<td>
										<? 
											switch($r['privilegios'])
											{
												case 0:
													echo "Usuario";
													break;
												case 1:
													echo "Master";
													break;
												case 101:
													echo "Admin";
													break;
											}
										?>
									</td>
								</tr>
							<?}
						?>
					</tbody>
				</table>
				<script>
					function updateData(input,field)
					{
						value = $(input).val();
						_saveData(field, value, <? echo $userid;?>,false);
					}
					function cambiaPass()
					{
						value=$("#user_<? echo $userid;?>_pass").val();
						pagina = "ucp";

						$(document).ready(function()
						{
							$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"passReset", value:value},
							function(output)
							{
								alert(output);
							});
						});
					}
				</script>
				<? break;
				
			case "savedata":
				if(isset($_POST['value']))
				{
					$bd->update('UPDATE users SET discord = \''.$_POST['value'].'\' WHERE id = '.$userid);
				}
				break;
			
			case "passReset":
				if(isset($_POST['value']))
				{
					$cont=rand(9,20);
					$semilla="";
					for($i=0;$i<$cont;$i++)
					{
						$semi=rand(0,9);
						$semilla=$semilla.$semi;
					}
					
					$pass=$_POST['value'];
					$GLOBALS['sesion']->set_password($userid,$pass,$semilla);
					
					echo "Contraseña cambiada";
				}
	}
}?>