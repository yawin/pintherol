<?
	if($priv < 100)
	{
		return;
	}
?>
<section class="content">
	<div class="row">
		<div class="col-md-6"><div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Juegos de rol</h3>
			</div>
			<div class="box-body">
				<div class="form-group box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th style="width: 10px">#</th>
								<th>Nombre</th>
							</tr>
							<?
								$query = "SELECT * FROM juegos ORDER BY nombre";
								foreach($bd->select($query) as $r)
								{?>
									<tr>
										<td><? echo $r['id'];?></td>
										<td><input OnChange="updateData('#system_<? echo $r['id'];?>_nombre',<? echo $r['id'];?>);" style="width:100%;" id="system_<? echo $r['id'];?>_nombre" value="<? echo $r['nombre'];?>"></td>
										<td>
											<select OnChange="updateSistema('#system_<? echo $r['id'];?>_sistema',<? echo $r['id'];?>);" id="system_<? echo $r['id'];?>_sistema" class="form-control">
												<? $query = "SELECT * FROM sistemas";
												foreach($bd->select($query) as $r2)
												{?>
													<option value="<? echo $r2['id'];?>" <? if($r2['id']==$r['sistema']){ echo "selected";}?>><? echo $r2['nombre'];?></option>
												<?}?>
											</select>
										</td>
									</tr>
								<?}
							?>
							<tr>
								<td>*</td>
								<td><input OnChange="saveNew();" style="width:100%;" id="system_0" placeholder="Introduce juego"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div></div>
	</div>
</section>
<script>
	function recarga()
	{
		pagina = "juegos";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin},
			function(output)
			{
				$('#contenido').html(output);
			});
		});
	}
	function saveNew()
	{
		value = $("#system_0").val();
		table = "juegos";
		_saveData("newdata", table, value, 0);
	}
	function updateData(field,id)
	{
		value = $(field).val();
		table = "juegos";
		_saveData("updatedata", table, value, id);
	}
	function updateSistema(field,id)
	{
		value = $(field).val();
		table = "juegos";
		_saveData("updatesistema", table, value, id);
	}
	function _saveData(orden, table, value, id)
	{
		pagina = "utils";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:orden, table:table, value:value, id:id},
			function(output)
			{
 				recarga();
			});
		});
	}
</script>