<?
	if($priv < 100)
	{
		return;
	}
?>
<section class="content">
	<div class="row">
		<div class="col-md-6"><div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Partidas</h3>
			</div>
			<div id="playTable" class="box-body">
			</div>
		</div></div>
		<div class="col-md-6">
			<div style="position: fixed;">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Datos de la partida</h3>
					</div>
					<div id="playData" class="box-body">
						<h4>No hay seleccionada ninguna partida</h4>
					</div>
				</div>
			</div>
		<div>
	</div>
</section>
<script>
	function reload()
	{
		pagina = "utils";
		plugin="acp";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"cargaPartidaTabla"},
			function(output)
			{
				$("#playTable").html(output);
			});
		});
	}
	
	/*function banhammer(userid,value)
	{
		_saveData("ban",value,userid,true);
	}
	
	function bloqhammer(userid,value)
	{
		_saveData("bloq",value,userid,true);
	}*/
	
	
	function cargaPartida(partidaid)
	{
		pagina = "utils";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"cargaPartida", id:partidaid},
			function(output)
			{
			$("#playData").html(output);
			});
		});
	}
	
	reload();
</script>
