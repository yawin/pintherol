<? $userid = $GLOBALS['sesion']->compruebases();
$priv = $GLOBALS['sesion']->getPrivileges($userid);

if($priv > 100)
{?>
	<li class="treeview active"><a><i class="glyphicon glyphicon-wrench"></i><span>Administración</span> <i class="glyphicon glyphicon-menu-down pull-right"></i></a>
		<ul class="treeview-menu">
			<li><a onclick="loadPage('sistemas', 'acp');"><i class="glyphicon glyphicon-equalizer"></i>Administrar sistemas</a></li>
			<li><a onclick="loadPage('juegos', 'acp');"><i class="glyphicon glyphicon-tower"></i>Administrar juegos</a></li>
			<li><a onclick="loadPage('usuarios', 'acp');"><i class="glyphicon glyphicon-user"></i>Administrar usuarios</a></li>
			<li><a onclick="loadPage('partidas', 'acp');"><i class=" glyphicon glyphicon-list-alt"></i>Administrar partidas</a></li>
		</ul>
	</li>
	<li><a onclick="loadPage('ruta', 'acp');"><i class="glyphicon glyphicon-signal"></i>Hoja de ruta</a></li>
<?}?>
<li><a onclick="loadPage('ucp', 'acp');"><i class="glyphicon glyphicon-user"></i>Mis datos</a></li>
