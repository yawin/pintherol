<?
	if(isset($_POST['version']))
	{
		$version = "v0.0";
		$query="SELECT version, subversion FROM hoja_de_ruta WHERE estado = 2 ORDER BY version DESC, subversion DESC LIMIT 1";
		foreach($bd->select($query) as $r){$version = 'v'.$r['version'].'.'.$r['subversion'];}
		echo $version;
		return;
	}
	
	if($priv < 100)
	{
		return;
	}
?>
<section class="content">
	<div class="row">
		<div class="col-md-8"><div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Hoja de ruta</h3>
			</div>
			<div id="usertable" class="box-body">
			</div>
		</div></div>
		<div class="col-md-4">
			<div style="position: fixed;">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Nueva tarea</h3>
					</div>
					<div id="userData" class="box-body">
						<p>
								<strong>Tarea</strong><br/>
								<input type="text" id="tarea" style="width:100%;" size="20" maxlength="255"/>
						</p>
						<table class="table">
							<tbody>
								<tr>
									<td style="width: 50px;">
										<b>Versión</b><br/>
										<input type="text" size="20" style="width:50px; height:25px;" id="version" maxlength="20"/>
									</td>
									<td style="width: 100px;">
										<b>Sub-Versión</b><br/>
										<input type="text" size="20" style="width:50px; height:25px;" id="subversion" maxlength="20"/>
									</td>
									<td>
										<br/>
										<i class="btn btn-success pull-right" OnClick="nuevaTarea();">Añadir</i>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="box hidden" id="formulario">
				</div>
			</div>
		<div>
	</div>
</section>
<script>
	function cargaEdit(tareaid)
	{
		pagina = "utils";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"cargaEdit", tareaid:tareaid},
			function(output)
			{
				$("#formulario").removeClass("hidden");
				$("#formulario").html(output);
			});
		});
	}
	function nuevaTarea()
	{
		version = $("#version").val();
		subversion = $("#subversion").val();
		tarea = $("#tarea").val();
		
		pagina = "utils";
		
		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"insertaNota", tarea:tarea, version:version, subversion:subversion},
			function(output)
			{
				reload();
				$("#version").val("");
				$("#subversion").val("");
				$("#tarea").val("");
			});
		});
	}

	function reload()
	{
		pagina = "utils";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"cargaNotas"},
			function(output)
			{
				$("#usertable").html(output);
			});
		});
	}
	reload();
	</script>
