<?
	if($priv < 100)
	{
		return;
	}
?>
<section class="content">
	<div class="row">
		<div class="col-md-8">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Usuarios</h3>
				</div>
				<div id="usertable" class="box-body">
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div style="position: fixed;">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Datos del usuario</h3>
					</div>
					<div id="userData" class="box-body">
						<h4>No hay seleccionado ning&uacute;n usuario</h4>
					</div>
				</div>
			</div>
		<div>
	</div>
</section>
<script>
	function reload()
	{
		pagina = "utils";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"cargaTabla"},
			function(output)
			{
				$("#usertable").html(output);
			});
		});
	}
	
	function banhammer(userid,value)
	{
		_saveData("ban",value,userid,true);
	}
	
	function bloqhammer(userid,value)
	{
		_saveData("bloq",value,userid,true);
	}
	
	function _saveData(field, value, id, rec)
	{
		pagina = "utils";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"banhammer", field:field, value:value, id:id},
			function(output)
			{
				reload();
			});
		});
	}
	
	function cargaUser(userid)
	{
		pagina = "utils";

		$(document).ready(function()
		{
			$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"cargaUser", id:userid},
			function(output)
			{
 				$("#userData").html(output);
			});
		});
	}
	
	reload();
</script>
