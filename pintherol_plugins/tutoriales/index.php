<section class="content">
	<div class="row">
		<div class="col-md-4">
			<div class="box box-solid ">
				<div class="box-header with-border">
					<h3 class="box-title">Tutoriales disponibles</h3>
				</div>
				<div class="box-body">
					<table class="table table-hover">
						<tbody>
							<? $query = "SELECT * FROM tutoriales WHERE rango_min <= ".$priv." ORDER BY nombre ASC";
							foreach($bd->select($query) as $r)
							{?>
								<tr OnClick="cargaVisor('<? echo $r['ruta'];?>');">
									<td style="width:200px;"><? echo $r['nombre'];?></td>
								</tr>
							<?}?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div id="visor"></div>
		</div>
		<script>
			function cargaVisor(ruta)
			{
				pagina = "utils";
				$(document).ready(function()
				{
					$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"cargaVisor", ruta:ruta},
					function(output)
					{
						$("#visor").html(output);
					});
				});
			}
			cargaVisor();
		</script>
	</div>
</section>