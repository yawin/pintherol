<? if($priv < 100){return;} ?>
<script>
	function enableTab(id)
	{
		var el = document.getElementById(id);
		el.onkeydown = function(e) 
		{
			if (e.keyCode === 9) 
			{ // tab was pressed
				// get caret position/selection
				var val = this.value,
				start = this.selectionStart,
				end = this.selectionEnd;

				// set textarea value to: text before caret + tab + text after caret
				this.value = val.substring(0, start) + '\t' + val.substring(end);

				// put caret at right position again
				this.selectionStart = this.selectionEnd = start + 1;

				// prevent the focus lose
				return false;
			}
		};
	}
</script>

<section class="content">
	<div class="row">
		<? $archivo = 0;
		$disponible = 0;
		$ruta = "";
		$nombre = "";

		if(isset($_POST['archv']))
		{
			$archivo = $_POST['archv'];
		}
		
		if($archivo == '')
		{?>
			<div class="col-md-6">
				<div class="box box-solid ">
					<div class="box-header with-border">
						<h3 class="box-title">Listado de tutoriales</h3>
					</div>
					<div class="box-body">
						<table class="table table-hover">
							<tbody>
								<tr>
									<th style="width:20px;">#</th>
									<th style="width:200px;">Título</th>
									<th>Ruta</th>
								</tr>
								<? $query = "SELECT * FROM tutoriales";
								foreach($bd->select($query) as $r)
								{?>
									<tr OnClick="cargaTutorial('<? echo $r['id'];?>');">
										<td><? echo $r['id'];?></td>
										<td><? echo $r['nombre'];?></td>
										<td><? echo $r['ruta'];?></td>
									</tr>
								<?}?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box box-solid ">
					<div class="box-header with-border">
						<h3 class="box-title">Formulario de creación</h3>
					</div>
					<div class="box-body">
						<table>
							<tbody>
								<tr>
									<td>&nbsp;</td>
									<th>
										<span class="pull-right">
											Título:&nbsp;
										</span>
									</th>
									<td>
										<input type="text" id="titulo" placeholder="Introduce título"/>
									</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<th>
										<span class="pull-right">
											Fichero:&nbsp;
										</span>
									</th>
									<td>
										<input type="text" id="path" placeholder="Introduce nombre de fichero"/>
									</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<th>
										<span class="pull-right">
											Rango mínimo:&nbsp;
										</span>
									</th>
									<td>
										<select id="rango_min" style="width: 100%;">
											<option value="0">Jugador</option>
											<option value="1">Master</option>
											<option value="101">Admin</option>
										</select>
									</td>
									<td>
										&nbsp;
										<button class="btn btn-default" title="Save" OnClick="crear();">
											<i class="fa fa-save">Crear tutorial</i>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Galería</h3>
						<button class="pull-right btn btn-default btn-xs" title="Save" OnClick="subeFile();">
							<i class="fa fa-save">Subir imagen</i>
						</button>
						<form id="cargador" style="margin-top: -50px;">
							<input name="archivo" class="pull-right" type="file"/>
						</form>
					</div>
					<div id="galeria" class="box-body" style="height: 300px; overflow: scroll;">
					</div>
				</div>
			</div>
		</div>
			<script>
				function crear()
				{
					ruta = $("#path").val();
					nombre = $("#titulo").val();
					rango_min = $("#rango_min").val();
					
					pagina = "utils";
					
					$(document).ready(function()
					{
						$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"crear", ruta:ruta, nombre:nombre, rango_min:rango_min},
						function(output)
						{
							cargaTutorial(output);
						});
					});
				}
				
				function cargaTutorial(id)
				{
					pagina = "writer";
					
					$(document).ready(function()
					{
						$.post('./loaderproxy.php',{content:pagina, plugin:plugin, archv:id},
						function(output)
						{
							$("#contenido").html(output);
						});
					});
				}
				
				function cargaGaleria()
				{
					pagina = "utils";
					
					$(document).ready(function()
					{
						$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"cargaGaleria"},
						function(output)
						{
							$("#galeria").html(output);
						});
					});
				}
				
				cargaGaleria();
				
				function subeFile()
				{
					path = "tutoriales";
					$(document).ready(function()
					{
						var formData = new FormData($("#cargador")[0]);
						
						$.ajax(
						{
							url: 'utils.php?func=load&path='+path,
							type: 'POST',
							data: formData,
							async: false,
							success: function(data) 
							{
								loadPage("writer",'tutoriales');
							},
							cache: false,
							contentType: false,
							processData: false
						});
					});
				}
			</script>
		<?}
		else
		{
			$query = "SELECT nombre, ruta FROM tutoriales WHERE id = ".$archivo;
			foreach($bd->select($query) as $sel)
			{

				$ruta = $sel['ruta'];
				$nombre = $sel['nombre'];
			}

			$disponible = 0;
			if(file_exists('./pintherol_plugins/tutoriales/tutoriales/'.$ruta.'.php'))
			{
				$disponible = 1;
			}?>
			<div class="col-md-6">
				<div style="position: fixed;">
					<div class="box box-solid">
						<div class="box-header with-border">
							<h3 class="box-title"><? echo ucwords($nombre);?><span style="font-size: 15px;"> (<? echo $ruta;?>)</span></h3>
						</div>
						<div class="box-body pad">
							<form>
								<textarea OnChange="guardaTutorial();" id="texto" class="textarea" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?if($disponible > 0)
								{
									$ficherito = './pintherol_plugins/tutoriales/tutoriales/'.$ruta.'.php';
									$gestor = fopen($ficherito, "r");
									$contenido = fread($gestor, filesize($ficherito));
									fclose($gestor);

									$search  = array('<', '>', 'ñ', 'á', 'é', 'í', 'ó', 'ú');
									$replace = array('&lt;', '&gt;', '&ntilde;', '&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;');

									echo str_replace($search, $replace, $contenido);
								}?></textarea>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Vista previa</h3>
					</div>
					<div id="visor" class="box-body pad">
					
					</div>
				</div>
			</div>
			<script>enableTab("texto");</script>
			<script>
				function cargaVisor()
				{
					ruta = '<? echo $ruta;?>';

					pagina = "utils";
					$(document).ready(function()
					{
						$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"cargaVisor", ruta:ruta},
						function(output)
						{
							$("#visor").html(output);
						});
					});
				}
				function guardaTutorial()
				{
					text = $("#texto").val();
					ruta = '<? echo $ruta;?>';

					pagina = "utils";
					$(document).ready(function()
					{
						$.post('./loaderproxy.php',{content:pagina, plugin:plugin, orden:"guardaTuto", ruta:ruta, texto:text},
						function(output)
						{
							cargaVisor();
						});
					});
				}

				function evento()
				{
					$(document).ready(function()
					{
						document.addEventListener("keyup", function(e)
						{
							guardaTutorial();
						});
					});
				}

				evento();
				cargaVisor();
			</script>
		<?}?>
	</div>
</section>