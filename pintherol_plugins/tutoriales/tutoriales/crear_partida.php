<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Cómo crear una partida</h3>
	</div>
	<div class="box-body">
		Para crear una partida:
		<ul>
			<li style="margin-top:10px;">Ve a la sección "Mis partidas"<br/>
			<img style="border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/info_partida.png"/></li>
			<li style="margin-top:10px;">Luego pulsa en el botón "Nueva"<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/crear_partida.png"/></li>
			<li style="margin-top:10px;">En el formulario que aparezca introduce la información de la partida<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/crear_partida_2.png"/></li>
			<li style="margin-top:10px;">Para completar el proceso, pulsa el botón "Crear"<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/crear_partida_3.png"/></li>
		</ul>
		<div class="alert alert-info">
			<h4><i class="icon glyphicon glyphicon-info-sign"></i>¡Recuerda!</h4>
			Si no encuentras el título del juego que vas a dirigir, habla con algún administrador para que lo añada a la lista.
		</div>
	</div>
</div>