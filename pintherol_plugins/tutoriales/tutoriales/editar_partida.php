<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Cómo editar una partida</h3>
	</div>
	<div class="box-body">
		Para editar una partida:
		<ul>
			<li style="margin-top:10px;">Ve a la sección "Mis partidas"<br/>
			<img style="border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/info_partida.png"/></li>
			<li style="margin-top:10px;">Luego pulsa en el botón con forma de ojo de la partida que quieras modificar<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/editar_partida.png"/></li>
			<li style="margin-top:10px;">Edita la información que desees en el siguiente formulario<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/editar_partida_2.png"/></li>
		</ul>
		<div class="alert alert-info">
			<h4><i class="icon glyphicon glyphicon-info-sign"></i>¡Recuerda!</h4>
			Cada vez que hagas un cambio, éste se guardará automáticamente (a menos que la conexión falle).
		</div>
		<div class="alert alert-danger">
			<h4><i class="icon glyphicon glyphicon-bullhorn"></i>¡Cuidado!</h4>
			El botón de borrar partida no te pedirá confirmación para borrarla. Si lo pulsas, adiós partida.
		</div>
	</div>
</div>