<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Cómo añadir documentos a una partida</h3>
	</div>
	<div class="box-body">
		Para añadir documentos a una partida:
		<ul>
			<li style="margin-top:10px;">Ve a la sección "Mis partidas"<br/>
			<img style="border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/info_partida.png"/></li>
			<li style="margin-top:10px;">Luego pulsa en el botón con forma de ojo de la partida a la que quieras añadir el documento<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/editar_partida.png"/></li>
			<li style="margin-top:10px;">Pulsa en el botón "Añadir documento"<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/añadir_documento.png"/></li>
			<li style="margin-top:10px;">Introduce el título con el que quieras identificar el documento. selecciona el documento y pulsa en el botón "Subir"<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/añadir_documento_2.png"/></li>
			<li style="margin-top:10px;">Una vez se haya subido, la lista de documentos se refrescará y te permitirá visualizarlos (con el botón con forma de nube) y eliminarlos (con el botón con forma de X)<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/añadir_documento_3.png"/></li>
		</ul>
		<div class="alert alert-info">
			<h4><i class="icon glyphicon glyphicon-info-sign"></i>¡Recuerda!</h4>
			Sólo los jugadores admitidos pueden ver los documentos.
		</div>
		<div class="alert alert-danger">
			<h4><i class="icon glyphicon glyphicon-bullhorn"></i>¡Cuidado!</h4>
			El botón de borrar documento no te pedirá confirmación para borrarla. Si lo pulsas, adiós documento.
		</div>
	</div>
</div>