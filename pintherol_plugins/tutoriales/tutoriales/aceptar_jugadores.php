<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Cómo aceptar jugadores en una partida</h3>
	</div>
	<div class="box-body">
		Para aceptar jugadores:
		<ul>
			<li style="margin-top:10px;">Ve a la sección "Mis partidas"<br/>
			<img style="border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/info_partida.png"/></li>
			<li style="margin-top:10px;">Luego pulsa en el botón con forma de ojo de la partida que quieras modificar<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/editar_partida.png"/></li>
			<li style="margin-top:10px;">A la derecha de la pantalla te aparecerán dos listados: el listado de jugadores aceptados y el listado de jugadores que solicitan entrar en la partida:<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/aceptar_jugadores.png"/></li>
			<li style="margin-top:10px;">Al pulsar sobre el jugador que quieres admitir en la partida, éste se unirá a la lista de jugadores admitidos:<br/>
			<img style="width:100%; border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/aceptar_jugadores_2.png"/></li>
		</ul>
		<div class="alert alert-warning">
			<h4><i class="icon glyphicon glyphicon-bullhorn"></i>¡Recuerda!</h4>
			Si pulsas en un jugador de la lista "Jugadores admitidos" lo desadmitirás de la partida.
		</div>
		<div class="alert alert-warning">
			<h4><i class="icon glyphicon glyphicon-bullhorn"></i>¡Recuerda!</h4>
			Aunque el contador de jugadores admitidos marca cuántos jugadores has admitido y cuántos jugadores máximos aceptas, el sistema no evita que admitas a más jugadores de los máximos permitidos. Es responsabilidad tuya respetar los límites que te marques.
		</div>
		<div class="alert alert-info">
			<h4><i class="icon glyphicon glyphicon-info-sign"></i>Ten en cuenta</h4>
			que los jugadores no admitidos sólo pueden acceder a la descripción básica de la partida y a la lista de jugadores admitidos. No pueden ver ni la lista de jugadores en espera de ser admitidos ni ningún documento que publiques en la partida.
		</div>
	</div>
</div>