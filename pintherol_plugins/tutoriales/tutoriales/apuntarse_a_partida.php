<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Cómo apuntarse a una partida</h3>
	</div>
	<div class="box-body">
		Apuntarse a una partida es muy sencillo:
		<ul>
			<li style="margin-top: 10px;">Primero: Seleccionas en el menú "Partidas abiertas"<br/>
			<img style=" border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/alta_juego_1.png"/></li>
			<li style="margin-top:10px;">Luego: Pulsas en el "+" de la partida a la que te quieras presentar<br/>
			<img src="http://pintherol.ddns.net/uploads/tutoriales/alta_juego_2.png" style="width:100%; border:1px solid grey;"/></li>
		</ul>
		Ahora ya sólo queda esperar a que el master te acepte como jugador. Puedes revisar el estado de tu solicitud en la sección "Mis partidas".
	</div>
</div>