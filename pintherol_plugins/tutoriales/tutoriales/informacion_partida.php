<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Ver la información de una partida</h3>
	</div>
	<div class="box-body">
		Para ver la información de una partida haz lo siguiente::
		<ul>
			<li style="margin-top: 10px;">Selecciona en el menú "Mis partidas"<br/>
			<img style="border:1px solid grey;" src="http://pintherol.ddns.net/uploads/tutoriales/info_partida.png"/></li>
			<li style="margin-top:10px;">Luego pulsa en el botón con forma de ojo de la partida que te interese<br/>
			<img src="http://pintherol.ddns.net/uploads/tutoriales/info_partida_2.png" style="width:100%; border:1px solid grey;"/></li>
		</ul>
		En esa pantalla podrás ver los datos generales de la partida, los jugadores aceptados y los datos que el master introduzca.
	</div>
</div>